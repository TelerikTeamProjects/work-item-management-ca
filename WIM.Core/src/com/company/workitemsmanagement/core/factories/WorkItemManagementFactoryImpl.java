package com.company.workitemsmanagement.core.factories;

import com.company.workitemsmanagement.models.board.BoardImpl;
import com.company.workitemsmanagement.models.common.Size;
import com.company.workitemsmanagement.models.contracts.*;
import com.company.workitemsmanagement.models.person.PersonImpl;
import com.company.workitemsmanagement.models.team.TeamImpl;
import com.company.workitemsmanagement.models.workitem.BugImpl;
import com.company.workitemsmanagement.models.workitem.FeedbackImpl;
import com.company.workitemsmanagement.models.workitem.StoryImpl;

import java.util.List;


public class WorkItemManagementFactoryImpl implements WorkItemManagementFactory {
    @Override
    public Board createBoard(String name) {
        return new BoardImpl(name);
    }

    @Override
    public Bug createBug(String title, String description, List<String> stepsToReproduce) {
        return new BugImpl(title, description, stepsToReproduce);
    }

    @Override
    public Feedback createFeedback(String title, String description) {
        return new FeedbackImpl(title, description);
    }

    @Override
    public Person createPerson(String name) {
        return new PersonImpl(name);
    }

    // Step 5.1.3.2.2 Create object Story
    @Override
    public Story createStory(String name, String description, Size size) {
        return new StoryImpl(name, description, size);
    }

    @Override
    public Team createTeam(String name) {
        return new TeamImpl(name);
    }
}
