package com.company.workitemsmanagement.core.factories;

import com.company.workitemsmanagement.models.contracts.Board;
import com.company.workitemsmanagement.models.contracts.Feedback;
import com.company.workitemsmanagement.models.contracts.Person;
import com.company.workitemsmanagement.models.contracts.Story;
import com.company.workitemsmanagement.models.contracts.*;
import com.company.workitemsmanagement.models.common.Size;

import java.util.List;

public interface WorkItemManagementFactory {
    Board createBoard(String name);

    Bug createBug(String title, String description, List<String> stepsToReproduce);

    Feedback createFeedback(String title, String description);

    Person createPerson(String name);

    Story createStory(String title, String description, Size size);

    Team createTeam(String name);
}
