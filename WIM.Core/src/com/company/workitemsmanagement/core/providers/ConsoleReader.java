package com.company.workitemsmanagement.core.providers;

import com.company.workitemsmanagement.core.contracts.Reader;

import java.util.Scanner;

public class ConsoleReader implements Reader {
    private final Scanner scanner;

    public ConsoleReader() {
        scanner = new Scanner(System.in);
    }

    // Step 4.1
    public String readLine() {
        return scanner.nextLine();
    }
}

