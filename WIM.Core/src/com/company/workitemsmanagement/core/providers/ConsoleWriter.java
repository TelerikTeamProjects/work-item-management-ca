package com.company.workitemsmanagement.core.providers;

import com.company.workitemsmanagement.core.contracts.Writer;

public class ConsoleWriter implements Writer {
    // Step 6.1 writeLine
    public void writeLine(String message) {
        System.out.println(message);
    }
}
