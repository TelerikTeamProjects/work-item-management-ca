package com.company.workitemsmanagement.core.providers;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.creation.*;
import com.company.workitemsmanagement.commands.listing.*;
import com.company.workitemsmanagement.commands.modification.*;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.contracts.Parser;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;

import java.util.ArrayList;
import java.util.List;

public class CommandParser implements Parser {
    private final WorkItemManagementFactory factory;
    private final Engine engine;

    public CommandParser(WorkItemManagementFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    // Step 5.1.1.1
    public Command parseCommand(String fullCommand) {
        String commandName = fullCommand.split("\\|")[0];
        return findCommand(commandName.trim());
    }

    @Override
    // Step 5.1.2.1
    public List<String> parseParameters(String fullCommand) {
        String[] commandParts = fullCommand.split("\\|");
        ArrayList<String> parameters = new ArrayList<>();
        for (int i = 1; i < commandParts.length; i++) {
            parameters.add(commandParts[i].trim());
        }
        return parameters;
    }

    // Step 5.1.1.2 Find command
    private Command findCommand(String commandName) {
        switch (commandName.toLowerCase()) {
            case "createboard":
                return new CreateBoardCommand(factory, engine);
            case "createbug":
                return new CreateBugCommand(factory, engine);
            case "createfeedback":
                return new CreateFeedbackCommand(factory, engine);
            case "createperson":
                return new CreatePersonCommand(factory, engine);
            case "createstory":
                return new CreateStoryCommand(factory, engine);
            case "createteam":
                return new CreateTeamCommand(factory, engine);
            case "addcomment":
                return new AddCommentCommand(factory, engine);
            case "addteammember":
                return new AddTeamMemberCommand(factory, engine);
            case "assignitem":
                return new AssignItemCommand(factory, engine);
            case "changepriority":
                return new ChangePriorityCommand(factory, engine);
            case "changerating":
                return new ChangeRatingCommand(factory, engine);
            case "changeseverity":
                return new ChangeSeverityCommand(factory, engine);
            case "changesize":
                return new ChangeSizeCommand(factory, engine);
            case "changestatus":
                return new ChangeStatusCommand(factory, engine);
            case "listitemsfilterbystatusandorassignee":
                return new ListItemsFilterByStatusAndOrAssigneeCommand(factory, engine);
            case "listitemsfilterbytype":
                return new ListItemsFilterByTypeCommand(factory, engine);
            case "listpersonworkitems":
                return new ListPersonWorkItemsCommand(factory, engine);
            case "listpeople":
                return new ListPeopleCommand(factory, engine);
            case "listteamboards":
                return new ListTeamBoardsCommands(factory, engine);
            case "listteammembers":
                return new ListTeamMembersCommand(factory, engine);
            case "listteams":
                return new ListTeamsCommand(factory, engine);
            case "sortitems":
                return new SortItemsCommand(factory, engine);
            case "unassignitem":
                return new UnassignItemCommand(factory, engine);
            case "listpersonactivity":
                return new ListPersonActivityCommand(factory, engine);
            case "listboardactivity":
                return new ListBoardActivityCommand(factory, engine);
            case "listteamsactivities":
                return new ListTeamActivityCommand(factory, engine);
            case "listitemhistory":
                return new ListItemHistory(factory, engine);
            default:
                throw new IllegalArgumentException(String.format("Invalid command %s.", commandName));
        }
    }
}
