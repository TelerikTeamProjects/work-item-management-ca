package com.company.workitemsmanagement.core.contracts;

public interface Reader {
    String readLine();
}
