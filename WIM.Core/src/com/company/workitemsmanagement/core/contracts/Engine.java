package com.company.workitemsmanagement.core.contracts;


import com.company.workitemsmanagement.models.contracts.Board;
import com.company.workitemsmanagement.models.contracts.Person;
import com.company.workitemsmanagement.models.contracts.Team;
import com.company.workitemsmanagement.models.contracts.WorkItem;

import java.util.List;
import java.util.Map;

public interface Engine {
    void start();

    List<Board> getBoards();

    Map<Integer, WorkItem> getWorkItems();

    Map<String, Person> getPersons();

    List<Team> getTeams();
}
