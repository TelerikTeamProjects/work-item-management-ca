package com.company.workitemsmanagement.core.contracts;

import com.company.workitemsmanagement.commands.contracts.Command;

import java.util.List;

public interface Parser {
    Command parseCommand(String fullCommand);

    List<String> parseParameters(String fullCommand);
}
