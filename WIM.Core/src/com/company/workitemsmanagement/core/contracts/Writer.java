package com.company.workitemsmanagement.core.contracts;

public interface Writer {
    void writeLine(String message);
}
