package com.company.workitemsmanagement.core;

import com.company.workitemsmanagement.models.contracts.Person;
import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.contracts.Parser;
import com.company.workitemsmanagement.core.contracts.Reader;
import com.company.workitemsmanagement.core.contracts.Writer;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.providers.CommandParser;
import com.company.workitemsmanagement.core.providers.ConsoleReader;
import com.company.workitemsmanagement.core.providers.ConsoleWriter;
import com.company.workitemsmanagement.models.person.PersonImpl;
import com.company.workitemsmanagement.models.contracts.Board;
import com.company.workitemsmanagement.models.contracts.Team;
import com.company.workitemsmanagement.models.contracts.WorkItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EngineImpl implements Engine {
    private static final String TERMINATION_COMMAND = "Exit";
    public static final Person DEFAULT_PERSON = new PersonImpl("Unassigned");
    public static int id = 0;

    private Reader reader;
    private Writer writer;
    private Parser parser;
    private final List<Board> boards;
    private final Map<Integer, WorkItem> workItems;
    private final Map<String, Person> persons;
    private final List<Team> teams;

    public EngineImpl(WorkItemManagementFactory factory) {
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
        parser = new CommandParser(factory, this);

        boards = new ArrayList<>();
        workItems = new HashMap<>();
        persons = new HashMap<>();
        teams = new ArrayList<>();

        id = 0;
    }

    @Override
    // Step 3.1
    public void start() {
        while (true) {
           /* // TODO restore try catch when finished
            String commandAsString = reader.readLine();
            if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                break;
            }
            // Step 5 Process command - Invoke method processCommand
            processCommand(commandAsString);
*/
            try {
                // Step 4 Read command - Invoke method readLine()
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                // Step 5 Process command - Invoke method processCommand()
                processCommand(commandAsString);
            } catch (Exception ex) {

                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());

            }
        }
    }

    // Step 5.1
    private void processCommand(String commandAsString) {
        /* [DG - Test test_02_create_person_validations] Input: " |"
           This validation is applied on a whole input string and doesn't handle empty command name */
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Command cannot be null or empty.");
        }

        // Step 5.1.1 Parse command - Invoke method parseCommand()
        Command command = parser.parseCommand(commandAsString);

        // Step 5.1.2 Parse parameters - Invoke method parseParameters()
        List<String> parameters = parser.parseParameters(commandAsString);

        // Step 5.1.3 Execute command - Invoke method execute()
        String executionResult = command.execute(parameters);

        // Step 6 Write Command result
        writer.writeLine(executionResult);
    }

    @Override
    public List<Board> getBoards() {
        return boards;
    }

    @Override
    public Map<Integer, WorkItem> getWorkItems() {
        return workItems;
    }

    @Override
    public Map<String, Person> getPersons() {
        return persons;
    }

    @Override
    public List<Team> getTeams() {
        return teams;
    }
}


