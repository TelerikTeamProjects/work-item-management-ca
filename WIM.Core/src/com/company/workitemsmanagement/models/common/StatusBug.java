package com.company.workitemsmanagement.models.common;

public enum StatusBug {
    ACTIVE("Active"),
    FIXED("Fixed");


    private String status;

    StatusBug(String status) {
        this.status = status;
    }

    @Override
    public String toString(){
        return status;
    }
}
