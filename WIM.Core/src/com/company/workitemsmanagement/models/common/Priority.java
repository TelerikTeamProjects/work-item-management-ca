package com.company.workitemsmanagement.models.common;

public enum Priority {
    HIGH("High"),
    MEDIUM("Medium"),
    LOW("Low");

    private String priority;

    Priority (String priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return priority;
    }
}

