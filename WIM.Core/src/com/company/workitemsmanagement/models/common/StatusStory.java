package com.company.workitemsmanagement.models.common;

public enum StatusStory {
    NOT_DONE("Not done"),
    IN_PROGRESS("In Progress"),
    DONE("Done");

    private String status;

    StatusStory (String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}
