package com.company.workitemsmanagement.models.common;

public enum Size {
    LARGE("Large"),
    MEDIUM("Medium"),
    SMALL("Small");

    // Step 5.1.3.1.1 getSize from ENUM Size
    private String size;

    Size (String size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return size;
    }
}
