package com.company.workitemsmanagement.models.common;

public enum StatusFeedback {
    NEW("New"),
    UNSCHEDULED("Unscheduled"),
    SCHEDULED("Scheduled"),
    DONE("Done");

    private String status;

    StatusFeedback (String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}
