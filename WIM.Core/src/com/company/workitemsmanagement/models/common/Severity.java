package com.company.workitemsmanagement.models.common;

public enum Severity {
    CRITICAL("Critical"),
    MAJOR("Major"),
    MINOR("Minor");

    private String severity;

    Severity (String severity) {
        this.severity = severity;
    }

    @Override
    public String toString() {
        return severity;
    }
}

