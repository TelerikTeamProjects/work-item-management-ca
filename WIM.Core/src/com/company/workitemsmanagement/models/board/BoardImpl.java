package com.company.workitemsmanagement.models.board;

import com.company.workitemsmanagement.models.contracts.Board;
import com.company.workitemsmanagement.models.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;

public class BoardImpl implements Board {
    private static final int NAME_MIN_LENGTH = 5;
    private static final int NAME_MAX_LENGTH = 10;
    private String name;
    private List<WorkItem> workItems;
    private List<String> activityHistory;

    public BoardImpl(String name) {
        setName(name);
        setWorkItems(workItems);
        setActivityHistory(activityHistory);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<WorkItem> getWorkItems() {
        return new ArrayList<>(workItems);
    }

    @Override
    public List<String> getActivityHistory() {
        return new ArrayList<>(activityHistory);
    }

    @Override
    public void addWorkItem(WorkItem item) {
        workItems.add(item);
    }

    // TODO check validations
    @Override
    public void addActivity(String activity) {
        activityHistory.add(activity);
    }

    @Override
    public String toString() {
        return String.format(" Board: %s", getName());
    }

    private void setName(String name) {
        if (name.length() < NAME_MIN_LENGTH || name.length() > NAME_MAX_LENGTH) {
            throw new IllegalArgumentException("Name should be between 5-10 characters.");
        }
        this.name = name;
    }

    private void setWorkItems(List<WorkItem> workItems) {
        this.workItems = new ArrayList<>();
    }

    private void setActivityHistory(List<String> activityHistory) {
        this.activityHistory = new ArrayList<>();
    }
}
