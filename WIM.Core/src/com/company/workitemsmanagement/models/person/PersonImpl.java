package com.company.workitemsmanagement.models.person;

import com.company.workitemsmanagement.models.contracts.Person;
import com.company.workitemsmanagement.models.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;

import static sun.security.pkcs11.wrapper.Functions.getId;

public class PersonImpl implements Person {
    private static final int NAME_MIN_LENGTH = 5;
    private static final int NAME_MAX_LENGTH = 10;
    private static final String INVALID_NAME_MESSAGE = "Name should be between %d-%d characters long.";

    private String name;
    private List<WorkItem> workItems;
    private List<String> activityHistory;

    // Step 9 D
    public PersonImpl(String name) {
        setName(name);
        setWorkItems(workItems);
        setActivityHistory(activityHistory);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<WorkItem> getWorkItems() {
        return new ArrayList<>(workItems);
    }

    public List<String> getActivityHistory() {
        return new ArrayList<>(activityHistory);
    }

    @Override
    public void addWorkItem(WorkItem workItem) {
        workItems.add(workItem);
    }

    public void removeWorkItem(WorkItem workItem) {
        workItems.remove(workItem);
    }

    @Override
    public void addActivity(String activity) {
        activityHistory.add(activity);
    }

    @Override
    public String toString() {
        return String.format(" Name: %s", getName());
    }

    private void setName(String name) {
        validateValueLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH, INVALID_NAME_MESSAGE);
        this.name = name;
    }

    private void setWorkItems(List<WorkItem> workItems) {
        this.workItems = new ArrayList<>();
    }

    private void setActivityHistory(List<String> activityHistory) {
        this.activityHistory = new ArrayList<>();
    }

    private void validateValueLength(String value, int minLength, int maxLength, String invalidValueMessage) {
        if (value.length() < minLength || value.length() > maxLength) {
            throw new IllegalArgumentException(String.format
                    (invalidValueMessage, minLength, maxLength));
        }
    }
}
