package com.company.workitemsmanagement.models.workitem;

import com.company.workitemsmanagement.models.common.StatusFeedback;
import com.company.workitemsmanagement.models.contracts.Feedback;

public class FeedbackImpl extends WorkItemBase implements Feedback {
    private StatusFeedback status;
    private int rating = 0;
    public static final int RATING_MIN = -10;
    public static final int RATING_MAX = 10;
    public static final String RATING_INVALID_MESSAGE = "Rating value should be between %s and %s.";

    public FeedbackImpl(String title, String description) {
        super(title, description);
        setStatus(StatusFeedback.NEW);
        setRating(rating);
    }

    @Override
    public StatusFeedback getStatus() {
        return status;
    }

    @Override
    public void setStatus(StatusFeedback status) {
        this.status = status;
    }

    @Override
    public int getRating() {
        return rating;
    }

    // TODO Implement rate item - add to Interface
    @Override
    public void setRating(int rating) {
        if (rating < RATING_MIN || rating > RATING_MAX) {
            throw new IllegalArgumentException(String.format(RATING_INVALID_MESSAGE, RATING_MIN, RATING_MAX));
        }
        this.rating = rating;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getPrintHeader());
        stringBuilder.append(super.toString());
        stringBuilder.append(getPrintFooter());
        return stringBuilder.toString();
    }

    @Override
    protected String getPrintHeader() {
        return " #Feedback ";
    }

    @Override
    protected String getPrintFooter() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("   Status: %s\r\n" +
                "   Rating: %s", getStatus(), getRating()));
        return stringBuilder.toString();
    }
}