package com.company.workitemsmanagement.models.workitem;

import com.company.workitemsmanagement.models.common.Priority;
import com.company.workitemsmanagement.models.contracts.ActionItem;
import com.company.workitemsmanagement.models.contracts.Person;

import static com.company.workitemsmanagement.core.EngineImpl.DEFAULT_PERSON;

public abstract class ActionItemBase extends WorkItemBase implements ActionItem {
    private Priority priority;
    private Person assignee;

    ActionItemBase(String title, String description, Priority priority) {
        super(title, description);
        setPriority(priority);
        setAssignee(DEFAULT_PERSON);
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public Person getAssignee() {
        return assignee;
    }

    // TODO Is null correct? Null is not correct
    // TODO Didi tests Unasssign
    public void unassignItem() {
        setAssignee(DEFAULT_PERSON);
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    @Override
    public void setAssignee(Person person) {
        this.assignee = person;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(super.toString());
        stringBuilder.append(String.format("   Priority: %s\r\n" +
                "   Assignee: %s\r\n", getPriority(), getAssignee().getName()));

        return stringBuilder.toString();
    }
}
