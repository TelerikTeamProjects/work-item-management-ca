package com.company.workitemsmanagement.models.workitem;

import com.company.workitemsmanagement.models.common.Priority;
import com.company.workitemsmanagement.models.common.Severity;
import com.company.workitemsmanagement.models.common.StatusBug;
import com.company.workitemsmanagement.models.contracts.Bug;

import java.util.ArrayList;
import java.util.List;

public class BugImpl extends ActionItemBase implements Bug {
    private StatusBug status;
    private Severity severity;
    private List<String> stepsToReproduce;

    public BugImpl(String title, String description, List<String > stepsToReproduce) {
        super(title, description, Priority.MEDIUM);
        setStepsToReproduce(stepsToReproduce);
        setStatus(StatusBug.ACTIVE);
        setSeverity(Severity.MINOR);
    }

    @Override
    public StatusBug getStatus() {
        return status;
    }

    @Override
    public void setStatus(StatusBug status) {
        this.status = status;
    }

    @Override
    public Severity getSeverity() {
        return severity;
    }

    @Override
    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getPrintHeader());
        stringBuilder.append(super.toString());
        stringBuilder.append(getPrintFooter());
        return stringBuilder.toString();
    }

    @Override
    protected String getPrintHeader() {
        return String.format(" #Bug ");
    }

    @Override
    protected String getPrintFooter() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("   Status: %s\r\n" +
                "   Severity: %s", getStatus(), getSeverity()));
        return stringBuilder.toString();
    }

    public void setStepsToReproduce(List<String> stepsToReproduce) {
        this.stepsToReproduce = new ArrayList<>();
    }
}