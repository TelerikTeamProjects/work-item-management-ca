package com.company.workitemsmanagement.models.workitem;

import com.company.workitemsmanagement.models.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;

import static com.company.workitemsmanagement.core.EngineImpl.id;

// Step 5.1.3.2.2.1
public abstract class WorkItemBase implements WorkItem {

    private int itemId;
    private String title;
    private String description;
    private List<String> comments;
    private List<String> history;

    WorkItemBase(String title, String description) {
        setTitle(title);
        setDescription(description);
        comments = new ArrayList<>();
        history = new ArrayList<>();
        id++;
        itemId = id;
    }

    public int getItemId() {
        return itemId;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<String> getComments() {
        return comments;
    }

    @Override
    public List<String> getHistory() {
        return new ArrayList<>(history);
    }

    @Override
    public void addHistory(String historyFile) {
        history.add(historyFile);
    }

    @Override
    public void addComment(String comment) {
        comments.add(comment);
    }

    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(String.format("ID: %s\r\n" +
                " #Title: %s\r\n" +
                "   Description: %s\r\n", getItemId(), getTitle(), getDescription()));

        return strBuilder.toString();
    }

    protected abstract String getPrintHeader();

    protected abstract String getPrintFooter();


    private void setTitle(String title) {
        validateValueLength(title, WorkItemConstants.TitleMinLength,
                WorkItemConstants.TitleMaxLength, WorkItemConstants.InvalidTitleMessage);
        this.title = title;
    }

    private void setDescription(String description) {
        validateValueLength(description, WorkItemConstants.DescriptionMinLength,
                WorkItemConstants.DescriptionMaxLength, WorkItemConstants.InvalidDescriptionMessage);
        this.description = description;
    }

    private void validateValueLength(String value, int minLength, int maxLength, String invalidValueMessage) {
        if (value.length() < minLength || value.length() > maxLength) {
            throw new IllegalArgumentException(String.format
                    (invalidValueMessage, minLength, maxLength));
        }
    }
}