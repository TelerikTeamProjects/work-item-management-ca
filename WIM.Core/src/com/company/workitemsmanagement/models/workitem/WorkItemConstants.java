package com.company.workitemsmanagement.models.workitem;

class WorkItemConstants {
    static final int TitleMinLength = 10;
    static final int TitleMaxLength = 50;
    static final String InvalidTitleMessage = "Title should be between %d-%d characters long.";
    static final int DescriptionMinLength = 10;
    static final int DescriptionMaxLength = 500;
    static final String InvalidDescriptionMessage = "Description should be between %d-%d characters long.";
}
