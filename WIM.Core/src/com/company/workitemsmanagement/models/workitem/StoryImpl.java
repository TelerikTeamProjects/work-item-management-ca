package com.company.workitemsmanagement.models.workitem;

import com.company.workitemsmanagement.models.common.Priority;
import com.company.workitemsmanagement.models.common.Size;
import com.company.workitemsmanagement.models.common.StatusStory;
import com.company.workitemsmanagement.models.contracts.Story;

public class StoryImpl extends ActionItemBase implements Story {
    private StatusStory status;
    private Size size;

    public StoryImpl(String title, String description, Size size) {
        super(title, description, Priority.MEDIUM);
        setStatus(StatusStory.NOT_DONE);
        setSize(size);
    }

    @Override
    public StatusStory getStatus() {
        return status;
    }

    @Override
    public void setStatus(StatusStory status) {
        this.status = status;
    }

    @Override
    public Size getSize() {
        return size;
    }

    @Override
    public void setSize(Size size) {
        this.size = size;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getPrintHeader());
        stringBuilder.append(super.toString());
        stringBuilder.append(getPrintFooter());
        return stringBuilder.toString();
    }

    @Override
    protected String getPrintHeader() {
        return " #Story ";
    }

    @Override
    protected String getPrintFooter() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("   Status: %s\r\n" +
                "   Size: %s", getStatus(), getSize()));
        return stringBuilder.toString();
    }
}

