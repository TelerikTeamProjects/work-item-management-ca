package com.company.workitemsmanagement.models.team;

import com.company.workitemsmanagement.models.contracts.Board;
import com.company.workitemsmanagement.models.contracts.Person;
import com.company.workitemsmanagement.models.contracts.Team;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TeamImpl implements Team {
    private static final int NAME_MIN_LENGTH = 5;
    private static final int NAME_MAX_LENGTH = 10;
    private static final String INVALID_NAME_MESSAGE = "Name should be between %d-%d characters long";

    private String name;
    private List<Person> members;
    private Map<String, Board> boards;

    public TeamImpl(String name) {
        setName(name);
        setMembers(members);
        setBoards(boards);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Person> getMembers() {
        return new ArrayList<>(members);
    }

    @Override
    public Map<String, Board> getBoards() {
        return new HashMap<>(boards);
    }

    @Override
    public void addMember(Person person) {
        members.add(person);
    }

    @Override
    public void addBoard(Board board) {
        boards.put(board.getName(), board);
    }

    // Print
    @Override
    public String toString() {
        return String.format(" Team %s", getName());
    }

    private void setName(String name) {
        validateValueLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH, INVALID_NAME_MESSAGE);
        this.name = name;
    }

    private void setMembers(List<Person> members) {
        this.members = new ArrayList<>();
    }

    private void setBoards(Map<String, Board> boards) {
        this.boards = new HashMap<>();
    }

    // Internal methods
    private void validateValueLength(String value, int minLength, int maxLength, String invalidValueMessage) {
        if (value.length() < minLength || value.length() > maxLength) {
            throw new IllegalArgumentException(String.format
                    (invalidValueMessage, minLength, maxLength));
        }
    }
}