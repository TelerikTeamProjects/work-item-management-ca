package com.company.workitemsmanagement.models.contracts;

import java.util.List;

public interface InfoLists {
    List<WorkItem> getWorkItems();

    List<String> getActivityHistory();

    void addWorkItem(WorkItem item);

    void addActivity(String activity);
}
