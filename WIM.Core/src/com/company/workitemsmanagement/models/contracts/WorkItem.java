package com.company.workitemsmanagement.models.contracts;

import java.util.List;

public interface WorkItem {
    int getItemId();

    String getTitle();

    String getDescription();

    List<String> getComments();

    List<String> getHistory();

    void addComment(String comment);

    void addHistory(String historyFile);
}