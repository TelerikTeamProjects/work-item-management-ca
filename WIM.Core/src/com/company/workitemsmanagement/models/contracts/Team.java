package com.company.workitemsmanagement.models.contracts;

import java.util.List;
import java.util.Map;

public interface Team {
    String getName();

    List<Person> getMembers();

    Map<String, Board> getBoards();

    void addMember(Person person);

    void addBoard(Board board);
}

