package com.company.workitemsmanagement.models.contracts;

public interface Person extends InfoLists {
    String getName();

    void removeWorkItem(WorkItem workItem);
}
