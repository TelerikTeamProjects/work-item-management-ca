package com.company.workitemsmanagement.models.contracts;

import com.company.workitemsmanagement.models.common.StatusFeedback;

public interface Feedback extends WorkItem {
    StatusFeedback getStatus();

    void setStatus(StatusFeedback feedback);

    int getRating();

    void setRating(int rating);
}
