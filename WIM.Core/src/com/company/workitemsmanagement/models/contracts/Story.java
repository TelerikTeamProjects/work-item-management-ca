package com.company.workitemsmanagement.models.contracts;

import com.company.workitemsmanagement.models.common.Size;
import com.company.workitemsmanagement.models.common.StatusStory;

public interface Story extends ActionItem {
    StatusStory getStatus();

    void setStatus(StatusStory status);

    Size getSize();

    void setSize(Size size);
}

