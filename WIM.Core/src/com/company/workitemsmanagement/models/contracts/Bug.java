package com.company.workitemsmanagement.models.contracts;

import com.company.workitemsmanagement.models.common.Severity;
import com.company.workitemsmanagement.models.common.StatusBug;

public interface Bug extends ActionItem {
    StatusBug getStatus();

    void setStatus(StatusBug status);

    Severity getSeverity();

    void setSeverity(Severity severity);
}
