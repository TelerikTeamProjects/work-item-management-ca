package com.company.workitemsmanagement.models.contracts;

import com.company.workitemsmanagement.models.common.Priority;

public interface ActionItem extends WorkItem {
    Priority getPriority();

    Person getAssignee();

    void setPriority(Priority priority);

    void setAssignee(Person person);

    void unassignItem();
}