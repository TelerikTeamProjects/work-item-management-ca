package com.company.workitemsmanagement.commands.contracts;

import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;

import java.util.List;

public interface Command {
    String execute(List<String> parameters);

    WorkItemManagementFactory getFactory();

    Engine getEngine();

    String getCurrentTimeStamp();
}
