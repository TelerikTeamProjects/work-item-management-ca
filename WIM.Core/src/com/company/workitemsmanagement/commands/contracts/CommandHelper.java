package com.company.workitemsmanagement.commands.contracts;

import java.util.List;

public class CommandHelper {

    public static void isCommandParametersCountValid(String className, List<String> parameters, int count) {
        if (parameters.size() != count)
            throw new IllegalArgumentException(String.format("Failed to parse %s parameters, command expects %s parameters.", className, count));
    }
}
