package com.company.workitemsmanagement.commands.contracts;

import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public abstract class CommandBase implements Command {
    private final WorkItemManagementFactory factory;
    private final Engine engine;

    // Step 5.1.1.3.1
    protected CommandBase(WorkItemManagementFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public abstract String execute(List<String> parameters);

    // Step 5.1.3.2.1
    @Override
    public WorkItemManagementFactory getFactory() {
        return factory;
    }

    @Override
    public Engine getEngine() {
        return engine;
    }


    public String getCurrentTimeStamp() {
        return new SimpleDateFormat(" #" + "yyyy-MM-dd HH:mm:ss " /* + " - " */ ).format(new Date());
    }
}

