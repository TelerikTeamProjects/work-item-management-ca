package com.company.workitemsmanagement.commands.listing;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.Person;
import com.company.workitemsmanagement.models.contracts.WorkItem;

import java.util.List;

public class ListPersonWorkItemsCommand extends CommandBase implements Command {
    // ListPersonWorkItems | Person 1

    public ListPersonWorkItemsCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        Person person;
        List<WorkItem> workItems;

        try {
            person = getEngine().getPersons().get(parameters.get(0));
            workItems = getEngine().getPersons().get(person.getName()).getWorkItems();
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ListPersonsWorkItems command parameters.");
        }

        if (workItems.size() == 0) {
            return String.format("%s's work items list:\r\n" +
                    " #No work items in the list", person.getName());
        }else {
            StringBuilder stringBuilder = new StringBuilder();
            for (WorkItem item : workItems) {
                stringBuilder.append(System.getProperty("line.separator"));
                stringBuilder.append(item.toString());
            }

            return String.format("%s's work items list:%s", person.getName(), stringBuilder);
        }

    }
}
