package com.company.workitemsmanagement.commands.listing;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;

import java.util.List;

public class ListItemHistory extends CommandBase implements Command {
    // Command - ListItemHistory - 1

    public ListItemHistory(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        if (getEngine().getWorkItems().size() < Integer.parseInt(parameters.get(0))) {
            throw new IllegalArgumentException(String.format("Workitem with ID%s does not exist", parameters.get(0)));
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < getEngine().getWorkItems().get(Integer.parseInt(parameters.get(0))).getHistory().size(); i++) {
            if (i == 0) {
                stringBuilder.append(getEngine().getWorkItems().get(Integer.parseInt(parameters.get(0))).getHistory().get(i));
            } else {
                stringBuilder.append("\n" + getEngine().getWorkItems().get(Integer.parseInt(parameters.get(0))).getHistory().get(i));
            }
        }

        return new String(stringBuilder);

    }
}
