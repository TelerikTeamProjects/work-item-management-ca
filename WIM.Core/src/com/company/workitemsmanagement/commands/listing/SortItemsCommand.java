package com.company.workitemsmanagement.commands.listing;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.*;

import java.util.*;
import java.util.stream.Collectors;

public class SortItemsCommand extends CommandBase implements Command {
    // Command - SortItems | Title

    public SortItemsCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        String result = null;
        if (parameters.size() != 1) {
            throw new IllegalArgumentException("Failed to parse SortItems command parameters.");
        }

        String filter = parameters.get(0);

        switch (filter.toLowerCase()) {
            case "title":
                result = getEngine().getWorkItems().values()
                        .stream().sorted(Comparator.comparing(WorkItem::getTitle))
                        .map(WorkItem::toString)
                        .collect(Collectors.joining(System.getProperty("line.separator")));
                break;
            case "priority":
                result = getEngine().getWorkItems().values().stream()
                        .filter(workItem -> workItem instanceof ActionItem)
                        .map((w1) -> (ActionItem) w1)
                        .sorted(Comparator.comparing(ActionItem::getPriority))
                        .map(WorkItem::toString)
                        .collect(Collectors.joining(System.getProperty("line.separator")));
                break;
            case "severity":
                result = getEngine().getWorkItems().values().stream()
                        .filter(workItem -> workItem instanceof Bug)
                        .map((w1) -> (Bug) w1)
                        .sorted(Comparator.comparing(Bug::getSeverity))
                        .map(WorkItem::toString)
                        .collect(Collectors.joining(System.getProperty("line.separator")));
                break;
            case "size":
                result = getEngine().getWorkItems().values().stream()
                        .filter(workItem -> workItem instanceof Story)
                        .map((w1) -> (Story) w1)
                        .sorted(Comparator.comparing(Story::getSize))
                        .map(WorkItem::toString)
                        .collect(Collectors.joining(System.getProperty("line.separator")));
                break;
            case "rating":
                result = getEngine().getWorkItems().values().stream()
                        .filter(workItem -> workItem instanceof Feedback)
                        .map((w1) -> (Feedback) w1)
                        .sorted(Comparator.comparing(Feedback::getRating))
                        .map(WorkItem::toString)
                        .collect(Collectors.joining(System.getProperty("line.separator")));
                break;
            default:
                throw new IllegalArgumentException(String.format("Filter %s is not supported.", filter));
        }

        return String.format("#Sort items by filter %s:\r\n%s", filter, result.isEmpty() ? " No items in this list." : result);
    }
}
