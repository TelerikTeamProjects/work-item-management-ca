package com.company.workitemsmanagement.commands.listing;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.common.StatusBug;
import com.company.workitemsmanagement.models.contracts.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ListItemsFilterByStatusAndOrAssigneeCommand extends CommandBase implements Command {
    // Command - ListItemsFilterByStatusAndOrAssignee | Done | Member

    public ListItemsFilterByStatusAndOrAssigneeCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        String result = null;
        String status;
        String assignee;
        String filter;

        if (parameters.size() != 2 || getEngine().getWorkItems().size() == 0) {
            throw new IllegalArgumentException("Failed to parse ListWorkItems command parameters.");
        } else {
            status = parameters.get(0);
            assignee = parameters.get(1);
        }

        if (status.equals("") && assignee != "") {
            filter = "assignee";
        }else if(status != "" && assignee.equals("")){
            filter = "status";
        }else{
            filter = "status and assignee";
        }

        switch (filter.toLowerCase()) {
            case "assignee":
                result = getEngine().getWorkItems().values()
                        .stream().filter(workItem -> (workItem instanceof ActionItem) &&
                                ((ActionItem)workItem).getAssignee().getName().equals(assignee))
                        .map(WorkItem::toString)
                        .collect(Collectors.joining(System.getProperty("line.separator")));
                break;
            case "status":
                result = getEngine().getWorkItems().values()
                        .stream()
                        .filter(workItem -> (workItem instanceof Bug)
                                && ((Bug) workItem).getStatus().toString().equals(status))
                        .filter(workItem -> (workItem instanceof Story)
                                && ((Story) workItem).getStatus().toString().equals(status))
                        .filter(workItem -> (workItem instanceof Feedback)
                                && ((Feedback) workItem).getStatus().toString().equals(status))
                        .map(WorkItem::toString)
                        .collect(Collectors.joining(System.getProperty("line.separator")));
                break;
            case "status and assignee":
                result = getEngine().getWorkItems().values()
                        .stream()
                        .filter(workItem -> (workItem instanceof Bug)
                                && ((Bug) workItem).getAssignee().getName().equals(assignee)
                                && ((Bug) workItem).getStatus().toString().equals(status))
                        .filter(workItem -> (workItem instanceof Story)
                                && ((Story) workItem).getAssignee().getName().equals(assignee)
                                && ((Story) workItem).getStatus().toString().equals(status))
                        .map(WorkItem::toString)
                        .collect(Collectors.joining(System.getProperty("line.separator")));
                break;
            default:
                throw new IllegalArgumentException(String.format("Filter %s is not supported.", filter));
        }

        return String.format("#List items by %s [%s %s]:\r\n%s", filter, status, assignee, result.isEmpty() ? " No items in this list." : result);
    }
}
