package com.company.workitemsmanagement.commands.listing;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.Board;
import com.company.workitemsmanagement.models.contracts.Team;

import java.util.List;

public class ListTeamBoardsCommands extends CommandBase implements Command {
    // Command - ListTeamBoards | 0

    public ListTeamBoardsCommands(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        Team team;
        StringBuilder stringBuilder;

        try {
            team = getEngine().getTeams().get(Integer.parseInt(parameters.get(0)));
            stringBuilder = new StringBuilder();
            stringBuilder.append("### Team boards list ###");

            if (team.getBoards().size() == 0) {
                stringBuilder.append(System.getProperty("line.separator"));
                stringBuilder.append(" No boards in this list.");
            }

            for (Board board : team.getBoards().values()) {
                stringBuilder.append(System.getProperty("line.separator"));
                stringBuilder.append(board.toString());
            }

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ListTeamBoards command parameters.");
        }

        return stringBuilder.toString();
    }
}
