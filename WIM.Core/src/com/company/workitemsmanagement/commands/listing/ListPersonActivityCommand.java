package com.company.workitemsmanagement.commands.listing;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.Person;

import java.time.Instant;
import java.util.List;

public class ListPersonActivityCommand extends CommandBase implements Command {
    // Command - ListActivity | 0

    public ListPersonActivityCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        Person person;

        if (parameters.size() != 1) {
            throw new IllegalArgumentException("Failed to parse ListPersonsActivities command parameters.");
        }

        if (!getEngine().getPersons().containsKey(parameters.get(0))) {
            throw new IllegalArgumentException(String.format("Person with name %s does not exist", parameters.get(0)));
        }

        person = getEngine().getPersons().get(parameters.get(0));

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("#Person %s activity list:\r\n", parameters.get(0)));
        for (int i = 0; i < person.getActivityHistory().size(); i++) {
            if (i == 0) {
                stringBuilder.append(person.getActivityHistory().get(i));
            } else {
                stringBuilder.append("\n" + person.getActivityHistory().get(i));
            }
        }

        return new String(stringBuilder);
    }
}

