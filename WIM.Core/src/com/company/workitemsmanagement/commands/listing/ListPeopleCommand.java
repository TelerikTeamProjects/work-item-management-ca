package com.company.workitemsmanagement.commands.listing;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;

import java.util.List;
import java.util.stream.Collectors;

public class ListPeopleCommand extends CommandBase implements Command {
    // Command - ListPeople

    public ListPeopleCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() > 0) {
            throw new IllegalArgumentException("Failed to parse ListPeople command parameters.");
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("### People list ###");

        if (getEngine().getPersons().size() == 0) {
            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append(" No persons in this list.");
        }

        List<String> sortedPersonList = getEngine().getPersons().keySet().stream().sorted().collect(Collectors.toList());

        for(int i = 0; i < sortedPersonList.size(); i++){
            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append(getEngine().getPersons().get(sortedPersonList.get(i)).toString());
        }
        return stringBuilder.toString();
    }
}
