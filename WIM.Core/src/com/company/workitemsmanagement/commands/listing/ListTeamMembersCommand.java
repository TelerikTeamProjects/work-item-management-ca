package com.company.workitemsmanagement.commands.listing;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.Person;
import com.company.workitemsmanagement.models.contracts.Team;

import java.util.List;

public class ListTeamMembersCommand extends CommandBase implements Command {
    // TODO implements Didi
    // TODO tests Niki

    public ListTeamMembersCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        Team team;
        StringBuilder stringBuilder;

        try {
            team = getEngine().getTeams().get(Integer.parseInt(parameters.get(0)));
            stringBuilder = new StringBuilder();
            stringBuilder.append("### Team members list ###");

            if (team.getMembers().size() == 0) {
                stringBuilder.append(System.getProperty("line.separator"));
                stringBuilder.append(" No members in this list.");
            }

            for (Person member : team.getMembers()) {
                stringBuilder.append(System.getProperty("line.separator"));
                stringBuilder.append(member.toString());
            }

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ListTeamBoards command parameters.");
        }

        return stringBuilder.toString();
    }
}

