package com.company.workitemsmanagement.commands.listing;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.Board;

import java.util.List;

public class ListBoardActivityCommand extends CommandBase implements Command {
    // Command - ListActivityBoard - 0

    public ListBoardActivityCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        Board board;

        if (parameters.size() != 1) {
            throw new IllegalArgumentException("Failed to parse ListBoardsActivities command parameters.");
        }

        if (getEngine().getBoards().size() - 1 < Integer.parseInt(parameters.get(0))) {
            throw new IllegalArgumentException(String.format("Board with ID %s does not exist", parameters.get(0)));
        }

        board = getEngine().getBoards().get(Integer.parseInt(parameters.get(0)));

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("#Board ID %s activity list:\r\n", parameters.get(0)));
        for (int i = 0; i < board.getActivityHistory().size(); i++) {
            if (i == 0) {
                stringBuilder.append(board.getActivityHistory().get(i));
            } else {
                stringBuilder.append("\r\n" + board.getActivityHistory().get(i));
            }
        }

        return new String (stringBuilder);
    }
}

