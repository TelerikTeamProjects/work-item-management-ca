package com.company.workitemsmanagement.commands.listing;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.Team;

import java.util.List;

public class ListTeamsCommand extends CommandBase implements Command {
    // Command - ListTeams
    // TODO implements Niki
    // TODO tests Didi

    public ListTeamsCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != 0) {
            throw new IllegalArgumentException("Failed to parse ListTeams command parameters.");
        }

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("#List teams:");

        if (getEngine().getTeams().size() == 0) {
            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append(" No teams in this list.");
        }

        for (Team team : getEngine().getTeams()) {
            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append(team.toString());
        }

        return stringBuilder.toString();
    }
}
