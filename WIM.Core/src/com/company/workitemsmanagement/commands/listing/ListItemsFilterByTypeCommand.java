package com.company.workitemsmanagement.commands.listing;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.WorkItem;

import java.util.List;

public class ListItemsFilterByTypeCommand extends CommandBase implements Command {
    // Command - ListItemsFilterByType | Bug

    public ListItemsFilterByTypeCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        String type = null;
        boolean applyFilter = true;
        StringBuilder stringBuilder = new StringBuilder();

        if (parameters.size() == 0) {
            applyFilter = false;
            stringBuilder.append("#List work items no filters:");
        } else if (parameters.size() == 1) {
            type = parameters.get(0);
            type = type.toLowerCase() + "impl";
            stringBuilder.append(String.format("#List work items filtered by type %s:", parameters.get(0)));
        } else {
            System.out.print("Failed to parse ListWorkItems command parameters.");
        }

        if (getEngine().getWorkItems().size() == 0) {
            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append(" No items in this list.");
        }

        for (WorkItem item : getEngine().getWorkItems().values()) {
            if (item.getClass().getSimpleName().toLowerCase().equals(type) && applyFilter) {
                stringBuilder.append(System.getProperty("line.separator"));
                stringBuilder.append(item.toString());
            } else if (!applyFilter) {
                stringBuilder.append(System.getProperty("line.separator"));
                stringBuilder.append(item.toString());
            }
        }

        return stringBuilder.toString();
    }
}

