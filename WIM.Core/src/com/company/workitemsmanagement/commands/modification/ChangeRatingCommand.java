package com.company.workitemsmanagement.commands.modification;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.Board;
import com.company.workitemsmanagement.models.contracts.Feedback;

import java.util.List;

public class ChangeRatingCommand extends CommandBase implements Command {
    // Command - ChangeRating | 0 | -9
    private static final int COMMAND_PARAMETERS_NUMBER = 2;

    public ChangeRatingCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        Feedback feedback;
        int rating;

        if (parameters.size() != COMMAND_PARAMETERS_NUMBER) {
            throw new IllegalArgumentException(String.format("Failed to parse ChangeRating command parameters, " +
                    "command expects %s parameters.", COMMAND_PARAMETERS_NUMBER));
        }

        try {
            feedback = (Feedback) getEngine().getWorkItems().get(Integer.parseInt(parameters.get(0)));
            rating = Integer.parseInt(parameters.get(1));
            feedback.setRating(rating);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeRating command parameters.");
        }

        // Add Item history
        feedback.addHistory(String.format(getCurrentTimeStamp() + "%s's Rating was changed to %s.", feedback.getTitle(), rating));

        // TODO rework with setRating or changeRating  ???
        return String.format("ID %s %s's rating was changed to %s.", parameters.get(0), feedback.getTitle(), rating);
    }

}
