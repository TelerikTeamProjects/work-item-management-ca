package com.company.workitemsmanagement.commands.modification;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.ActionItem;
import com.company.workitemsmanagement.models.contracts.Person;

import java.util.List;

public class UnassignItemCommand extends CommandBase implements Command {
    private static final int COMMAND_PARAMETERS_NUMBER = 1;
    // Command - UnassignItem | 2

    public UnassignItemCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        ActionItem workItem;
        Person assignee;

        if (parameters.size() != COMMAND_PARAMETERS_NUMBER) {
            throw new IllegalArgumentException(String.format("Failed to parse UnassignItem command parameters, " +
                    "command expects %s parameters.", COMMAND_PARAMETERS_NUMBER));
        }

        try {
            workItem = (ActionItem) getEngine().getWorkItems().get(Integer.parseInt(parameters.get(0)));
        } catch (Exception ex) {
            throw new IllegalArgumentException(String.format("Work Item ID %s is currently unassigned.", parameters.get(0)));
        }

        if (workItem == null) {
            throw new IllegalArgumentException(String.format("Work Item with ID %s does not exist.", parameters.get(0)));
        }

        assignee = workItem.getAssignee();

        if (assignee.getName() != "Unassigned") {
            assignee.removeWorkItem(workItem);
            workItem.unassignItem();
        } else {
            throw new IllegalArgumentException(String.format("Work Item ID %s is currently unassigned.", parameters.get(0)));
        }


        // Add person activity
        assignee.addActivity(String.format(getCurrentTimeStamp() + "Workitem ID %s unassigned from Person %s",
                parameters.get(0), assignee.getName()));

        // Add Item history
        workItem.addHistory(String.format(getCurrentTimeStamp() + "Workitem ID %s unassigned from Person %s",
                parameters.get(0), assignee.getName()));


        return String.format("Item ID %s %s was unassigned from Person %s.",
                parameters.get(0), workItem.getTitle(), assignee.getName());
    }
}

