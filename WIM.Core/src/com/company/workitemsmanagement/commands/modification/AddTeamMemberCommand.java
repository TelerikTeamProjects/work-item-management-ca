package com.company.workitemsmanagement.commands.modification;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.Person;
import com.company.workitemsmanagement.models.contracts.Team;

import java.util.List;

public class AddTeamMemberCommand extends CommandBase implements Command {
    // Command - AddTeamMember | 0 | Member

    public AddTeamMemberCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        Team team;
        Person member;

        try {
            team = getEngine().getTeams().get(Integer.parseInt(parameters.get(0)));
            member = getEngine().getPersons().get(parameters.get(1));
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse AddTeamMember command parameters.");
        }

        if(member == null){
            throw new IllegalArgumentException("Failed to parse AddTeamMember command parameters.");
        }else {
            team.addMember(member);
        }

        // Add person activity
        member.addActivity(String.format(getCurrentTimeStamp() + "Person %s joined team %s", member.getName(), team.getName()));

        return String.format("%s was added to team %s ID %s.", member.getName(), team.getName(), getEngine().getTeams().indexOf(team));
    }
}
