package com.company.workitemsmanagement.commands.modification;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.common.Severity;
import com.company.workitemsmanagement.models.contracts.Bug;

import java.util.List;

public class ChangeSeverityCommand extends CommandBase implements Command {
    // Command ChangeSeverity | 0 | Critical

    public ChangeSeverityCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        Bug bug;
        Severity severity;

        try {
            bug = (Bug) getEngine().getWorkItems().get(Integer.parseInt(parameters.get(0)));
            severity = Severity.valueOf(parameters.get(1).toUpperCase());
            bug.setSeverity(severity);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeSeverity command parameters.");
        }

        // Add Item history
        bug.addHistory(String.format(getCurrentTimeStamp() + "%s's Severity was changed to %s", bug.getTitle(), bug.getSeverity()));

        return String.format("ID %s %s's severity was changed to %s.", parameters.get(0), bug.getTitle(), bug.getSeverity());
    }
}

