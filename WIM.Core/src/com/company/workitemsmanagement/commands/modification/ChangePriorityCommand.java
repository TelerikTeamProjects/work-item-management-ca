package com.company.workitemsmanagement.commands.modification;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.common.Priority;
import com.company.workitemsmanagement.models.contracts.ActionItem;

import java.util.List;

public class ChangePriorityCommand extends CommandBase implements Command {
    // Command - ChangePriority | 0 | High

    public ChangePriorityCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        ActionItem actionItem;
        Priority priority;

        try {
            actionItem = (ActionItem) getEngine().getWorkItems().get(Integer.parseInt(parameters.get(0)));
            priority = Priority.valueOf(parameters.get(1).toUpperCase());
            actionItem.setPriority(priority);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangePriority command parameters.");
        }

        // Add Item history
        actionItem.addHistory(String.format(getCurrentTimeStamp() + "%s's Priority was changed to %s.",
                actionItem.getTitle(), actionItem.getPriority()));

        return String.format("ID %s %s's priority was changed to %s.",
                parameters.get(0), actionItem.getTitle(), actionItem.getPriority());
    }
}


