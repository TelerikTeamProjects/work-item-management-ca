package com.company.workitemsmanagement.commands.modification;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.*;

import java.util.List;

public class AssignItemCommand extends CommandBase implements Command {
    // Command - AssignItem | 0 | Person

    public AssignItemCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        WorkItem workItem;
        Person person;
        boolean isPersonMemberOfTeamOwnerOfItem = false;
        String result;

        try {
            workItem = getEngine().getWorkItems().get(Integer.parseInt(parameters.get(0)));
            person = getEngine().getPersons().get(parameters.get(1));

            for (Team team : getEngine().getTeams()) {
                if (team.getMembers().contains(person)) {
                    for (Board board : team.getBoards().values()) {
                        if (board.getWorkItems().contains(workItem)) {
                            isPersonMemberOfTeamOwnerOfItem = true;
                        } else {
                            break;
                        }
                    }
                } else {
                    break;
                }
            }

            if (isPersonMemberOfTeamOwnerOfItem) {
                ((ActionItem) workItem).setAssignee(person);
                person.addWorkItem(workItem);

                result = String.format("Work Item %s with ID %s was assigned to person %s.",
                        workItem.getTitle(), parameters.get(0), person.getName());

                // Add person activity
                person.addActivity(String.format(getCurrentTimeStamp() + "Workitem ID %s assigned to Person %s",
                        parameters.get(0), person.getName()));

                // Add Item history
                workItem.addHistory(String.format(getCurrentTimeStamp() + "Workitem ID %s assigned to Person %s",
                        parameters.get(0), person.getName()));

            } else {
                result = "Member and item don't belong to the same team.";
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse AssignItem command parameters.");
        }

        return result;
    }
}
