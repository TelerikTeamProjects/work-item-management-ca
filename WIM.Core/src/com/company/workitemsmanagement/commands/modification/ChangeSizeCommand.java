package com.company.workitemsmanagement.commands.modification;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.common.Size;
import com.company.workitemsmanagement.models.contracts.Story;

import java.util.List;

public class ChangeSizeCommand extends CommandBase implements Command {
    // Command - ChangeSize | 1 | Large

    public ChangeSizeCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        Story story;
        Size size;

        try {
            story = (Story) getEngine().getWorkItems().get(Integer.parseInt(parameters.get(0)));
            size = getSize(parameters.get(1));
            story.setSize(size);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeSize command parameters.");
        }

        // Add Item history
        story.addHistory(String.format(getCurrentTimeStamp() + "%s's Size was changed to %s", story.getTitle(), story.getSize()));

        return String.format("ID %s %s's size was changed to %s.", parameters.get(0), story.getTitle(), story.getSize());
    }

    private Size getSize(String sizeAsString) {
        return Size.valueOf(sizeAsString.toUpperCase());
    }

}
