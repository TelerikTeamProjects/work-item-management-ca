package com.company.workitemsmanagement.commands.modification;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.WorkItem;

import java.util.List;

public class AddCommentCommand extends CommandBase implements Command {
    // Command - AddComment | 0 | Comment

    public AddCommentCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        WorkItem workItem;
        String comment;

        try {
            workItem = getEngine().getWorkItems().get(Integer.parseInt(parameters.get(0)));
            comment = parameters.get(1);;
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse AddComment command parameters.");
        }

        if(workItem == null){
            throw new IllegalArgumentException("Failed to parse AddComment command parameters.");
        }else {
            workItem.addComment(comment);
        }

        // Add Item history
        workItem.addHistory(String.format(getCurrentTimeStamp() + "Comment was added to workitem %s ID%s.",
                workItem.getTitle(), parameters.get(0)));
        return String.format("Comment is added to work item with ID %s and title %s.", workItem.getItemId(), workItem.getTitle());
    }
}
