package com.company.workitemsmanagement.commands.modification;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.common.*;
import com.company.workitemsmanagement.models.workitem.*;

import java.util.List;

public class ChangeStatusCommand extends CommandBase implements Command {
    // Command - ChangeStatus | 0 | Fixed

    public ChangeStatusCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        int itemId;
        StatusBug bugStatus;
        StatusStory storyStatus;
        StatusFeedback feedbackStatus;
        String item;
        String title = "";
        String status = "";

        try {
            itemId = Integer.parseInt(parameters.get(0));
            item = getEngine().getWorkItems().get(itemId).getClass().getSimpleName();
            switch (item) {
                case "BugImpl":
                    bugStatus = StatusBug.valueOf(parameters.get(1).toUpperCase());
                    ((BugImpl) getEngine().getWorkItems().get(itemId)).setStatus(bugStatus);
                    title = getEngine().getWorkItems().get(itemId).getTitle();
                    status = ((BugImpl) getEngine().getWorkItems().get(itemId)).getStatus().toString();

                    // Add Item history
                    getEngine().getWorkItems().get(itemId).addHistory(String.format(getCurrentTimeStamp()
                                    + "%s's Status was changed to %s",
                            getEngine().getWorkItems().get(itemId).getTitle(),
                            ((BugImpl) getEngine().getWorkItems().get(itemId)).getStatus()));
                    break;
                case "StoryImpl":
                    storyStatus = StatusStory.valueOf(parameters.get(1).toUpperCase().replace(" ", "_"));
                    ((StoryImpl) getEngine().getWorkItems().get(itemId)).setStatus(storyStatus);
                    title = getEngine().getWorkItems().get(itemId).getTitle();
                    status = ((StoryImpl) getEngine().getWorkItems().get(itemId)).getStatus().toString();

                    // Add Item history
                    getEngine().getWorkItems().get(itemId).addHistory(String.format(getCurrentTimeStamp()
                                    + "%s's Status was changed to %s",
                            getEngine().getWorkItems().get(itemId).getTitle(),
                            ((StoryImpl) getEngine().getWorkItems().get(itemId)).getStatus()));
                    break;
                case "FeedbackImpl":
                    feedbackStatus = StatusFeedback.valueOf(parameters.get(1).toUpperCase());
                    ((FeedbackImpl) getEngine().getWorkItems().get(itemId)).setStatus(feedbackStatus);
                    title = getEngine().getWorkItems().get(itemId).getTitle();
                    status = ((FeedbackImpl) getEngine().getWorkItems().get(itemId)).getStatus().toString();

                    // Add Item history
                    getEngine().getWorkItems().get(itemId).addHistory(String.format(getCurrentTimeStamp()
                            + "%s's Status was changed to %s",
                            getEngine().getWorkItems().get(itemId).getTitle(),
                            ((FeedbackImpl) getEngine().getWorkItems().get(itemId)).getStatus()));
                    break;
            }

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeStatus command parameters.");
        }

        return String.format("Id %s %s status was changed to %s.", itemId, title, status);
    }
}
