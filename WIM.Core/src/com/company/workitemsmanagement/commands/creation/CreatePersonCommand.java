package com.company.workitemsmanagement.commands.creation;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.Person;

import java.util.List;

import static com.company.workitemsmanagement.commands.contracts.CommandHelper.isCommandParametersCountValid;

public class CreatePersonCommand extends CommandBase implements Command {
    // Command - CreatePerson | Person
    private static final int COMMAND_PARAMETERS_NUMBER = 1;

    public CreatePersonCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        String name;
        Person person;

        isCommandParametersCountValid(getClass().getSimpleName(), parameters, COMMAND_PARAMETERS_NUMBER);
        name = parameters.get(0);

        person = getFactory().createPerson(name);

        if (getEngine().getPersons().containsKey(person.getName())) {
            throw new IllegalArgumentException("Person's name should be unique.");
        }

        getEngine().getPersons().put(person.getName(), person);

        // Add person activity
        person.addActivity(String.format(getCurrentTimeStamp() + "Person %s was created", person.getName()));

        return String.format("Person with name %s was created.", person.getName());
    }
}

