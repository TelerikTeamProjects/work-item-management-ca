package com.company.workitemsmanagement.commands.creation;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.Board;
import com.company.workitemsmanagement.models.contracts.Team;

import java.util.List;

import static com.company.workitemsmanagement.commands.contracts.CommandHelper.isCommandParametersCountValid;

public class CreateBoardCommand extends CommandBase implements Command {
    // Command - CreateBoard | Board 1 | 0
    private static final int COMMAND_PARAMETERS_NUMBER = 2;

    public CreateBoardCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        String name;
        Team team;
        Board board;

        isCommandParametersCountValid(getClass().getSimpleName(), parameters, COMMAND_PARAMETERS_NUMBER);

        if (getEngine().getTeams().size() - 1 < Integer.parseInt(parameters.get(1))) {
            throw new IllegalArgumentException(String.format("Team with ID %s does not exist", parameters.get(1)));
        }

        name = parameters.get(0);
        team = getEngine().getTeams().get(Integer.parseInt(parameters.get(1)));

        board = getFactory().createBoard(name);
        getEngine().getBoards().add(board);

        if (team.getBoards().containsKey(board.getName())) {
            throw new IllegalArgumentException(String.format("Board with name %s already exists.", board.getName()));
        }

        team.addBoard(board);

        // Add board activity
        board.addActivity(String.format(getCurrentTimeStamp() + "Board with title %s ID %s was created",
                board.getName(), getEngine().getBoards().size() - 1));

        return String.format("Board with name %s and ID %d was created.", board.getName(), getEngine().getBoards().size() - 1);
    }
}

