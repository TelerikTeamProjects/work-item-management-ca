package com.company.workitemsmanagement.commands.creation;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.Board;
import com.company.workitemsmanagement.models.contracts.Feedback;

import java.util.List;

import static com.company.workitemsmanagement.commands.contracts.CommandHelper.isCommandParametersCountValid;

public class CreateFeedbackCommand extends CommandBase implements Command {
    // Command - CreateFeedback | Test Feedback | Feedback Description | 0
    private static final int COMMAND_PARAMETERS_NUMBER = 3;

    public CreateFeedbackCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        String title;
        String description;
        Board board;
        Feedback feedback;

        isCommandParametersCountValid(getClass().getSimpleName(), parameters, COMMAND_PARAMETERS_NUMBER);

        if (getEngine().getBoards().size() - 1 < Integer.parseInt(parameters.get(2))) {
            throw new IllegalArgumentException(String.format("Board with ID%s does not exist", parameters.get(2)));
        }

        title = parameters.get(0);
        description = parameters.get(1);
        board = getEngine().getBoards().get(Integer.parseInt(parameters.get(2)));

        feedback = getFactory().createFeedback(title, description);
        getEngine().getWorkItems().put(feedback.getItemId(), feedback);
        board.addWorkItem(feedback);

        // Add Item history
        feedback.addHistory(String.format(getCurrentTimeStamp() + "Feedback workItem %s ID %s was added to Board %s ID %s",
                parameters.get(0), feedback.getItemId(), board.getName(), parameters.get(2)));

        return String.format("Feedback with title %s and ID %d was created.", feedback.getTitle(), feedback.getItemId());
    }
}
