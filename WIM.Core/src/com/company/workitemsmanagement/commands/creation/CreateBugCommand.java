package com.company.workitemsmanagement.commands.creation;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.Board;
import com.company.workitemsmanagement.models.contracts.Bug;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.company.workitemsmanagement.commands.contracts.CommandHelper.isCommandParametersCountValid;

public class CreateBugCommand extends CommandBase implements Command {
    // Command - CreateBug | Bug Title13 | Bug Description | Step1; Step2 | 0
    private static final int COMMAND_PARAMETERS_NUMBER = 4;

    public CreateBugCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    public String execute(List<String> parameters) {
        String title;
        String description;
        List<String> stepsToReproduce;
        Board board;
        Bug bug;

        isCommandParametersCountValid(getClass().getSimpleName(), parameters, COMMAND_PARAMETERS_NUMBER);

        if (getEngine().getBoards().size() - 1 < Integer.parseInt(parameters.get(3))) {
            throw new IllegalArgumentException(String.format("Board with ID%s does not exist", parameters.get(3)));
        }

        title = parameters.get(0);
        description = parameters.get(1);
        stepsToReproduce = Arrays.stream(parameters.get(2).trim().split(";")).collect(Collectors.toList());
        board = getEngine().getBoards().get(Integer.parseInt(parameters.get(3)));

        bug = getFactory().createBug(title, description, stepsToReproduce);
        getEngine().getWorkItems().put(bug.getItemId(), bug);
        board.addWorkItem(bug);

        // Add Item history
        bug.addHistory(String.format(getCurrentTimeStamp() + "Bug workItem %s ID%s was added to Board %s ID %s",
                parameters.get(0), bug.getItemId(), board.getName(), parameters.get(3)));

        return String.format("Bug with title %s and ID %d was created.", bug.getTitle(), bug.getItemId());
    }
}
