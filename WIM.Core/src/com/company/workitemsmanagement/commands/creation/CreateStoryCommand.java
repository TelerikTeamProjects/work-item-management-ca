package com.company.workitemsmanagement.commands.creation;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.Board;
import com.company.workitemsmanagement.models.contracts.Story;
import com.company.workitemsmanagement.models.common.Size;

import java.util.List;

import static com.company.workitemsmanagement.commands.contracts.CommandHelper.isCommandParametersCountValid;

public class CreateStoryCommand extends CommandBase implements Command {
    // Command - CreateStory | Story Title | Story Description | Medium | 0
    private static final int COMMAND_PARAMETERS_NUMBER = 4;

    // Step 5.1.1.3 Create Story command
    public CreateStoryCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    // Step 5.1.3.1 Execute Story command
    @Override
    public String execute(List<String> parameters) {
        String title;
        String description;
        Size size;
        Board board;
        Story story;

        isCommandParametersCountValid(getClass().getSimpleName(), parameters, COMMAND_PARAMETERS_NUMBER);

        if (getEngine().getBoards().size() - 1 < Integer.parseInt(parameters.get(3))) {
            throw new IllegalArgumentException(String.format("Board with ID%s does not exist", parameters.get(3)));
        }

        title = parameters.get(0);
        description = parameters.get(1);
        board = getEngine().getBoards().get(Integer.parseInt(parameters.get(3)));

        try {
            size = Size.valueOf(parameters.get(2).toUpperCase());
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateStory command parameters.");
        }

        // Step 5.1.3.2 Create Story
        story = getFactory().createStory(title, description, size);
        getEngine().getWorkItems().put(story.getItemId(), story);
        board.addWorkItem(story);

        // Add Item history
        story.addHistory(String.format(getCurrentTimeStamp() + "Story workItem %s ID %s was added to Board %s ID %s",
                parameters.get(0), story.getItemId(), board.getName(), parameters.get(3)));

        return String.format("Story with title %s and ID %d was created.", story.getTitle(), story.getItemId());
    }
}

