package com.company.workitemsmanagement.commands.creation;

import com.company.workitemsmanagement.commands.contracts.Command;
import com.company.workitemsmanagement.commands.contracts.CommandBase;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.models.contracts.Team;

import java.util.List;

import static com.company.workitemsmanagement.commands.contracts.CommandHelper.isCommandParametersCountValid;

public class CreateTeamCommand extends CommandBase implements Command {
    // Command - CreateTeam | Team08
    private static final int COMMAND_PARAMETERS_NUMBER = 1;

    public CreateTeamCommand(WorkItemManagementFactory factory, Engine engine) {
        super(factory, engine);
    }

    @Override
    public String execute(List<String> parameters) {
        String name;

        isCommandParametersCountValid(getClass().getSimpleName(), parameters, COMMAND_PARAMETERS_NUMBER);

        name = parameters.get(0);

        Team team = getFactory().createTeam(name);
        getEngine().getTeams().add(team);

        // Add team activity
        // ...

        return String.format("Team with name %s and ID %d was created.", team.getName(), getEngine().getTeams().size() - 1);
    }
}
