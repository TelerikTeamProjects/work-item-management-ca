package com.company;

import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;

class WorkItemManagement {
    public static void main(String[] args) {
        // Step 1 Initialize new instance of WorkItemManagementFactory
        WorkItemManagementFactory factory = new WorkItemManagementFactoryImpl();

        // Step 2 Initialize new instance of Engine
        Engine engine = new EngineImpl(factory);

        // Step 3 Invoke method start
        engine.start();
    }
}
