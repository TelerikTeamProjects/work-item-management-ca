package com.company.workitemmanagement.unittests.workitemmanagementfactory;

import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import com.company.workitemsmanagement.models.contracts.Person;
import org.junit.Assert;
import org.junit.Test;

public class CreatePerson_Should {

    @Test
    public void createPerson_Should_ReturnInstanceOfTypePerson() {
        WorkItemManagementFactoryImpl factory = new WorkItemManagementFactoryImpl();
        Person person = factory.createPerson("Person One");
        Assert.assertTrue(person instanceof Person);
    }
}
