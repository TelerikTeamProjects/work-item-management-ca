package com.company.workitemmanagement.unittests.workitemmanagementfactory;

import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import com.company.workitemsmanagement.models.contracts.Board;
import org.junit.Assert;
import org.junit.Test;

public class CreateBoard_Should {

    @Test
    public void createBoard_Should_ReturnInstanceOfTypeBoard() {
        WorkItemManagementFactoryImpl factory = new WorkItemManagementFactoryImpl();
        Board board = factory.createBoard("Board Two");
        Assert.assertTrue(board instanceof Board);
    }
}
