package com.company.workitemmanagement.unittests.workitemmanagementfactory;

import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import com.company.workitemsmanagement.models.common.Size;
import com.company.workitemsmanagement.models.contracts.Story;
import org.junit.Assert;
import org.junit.Test;

public class CreateStory_Should {

    @Test
    public void createStory_Should_ReturnInstanceOfTypeStory() {
        WorkItemManagementFactoryImpl factory = new WorkItemManagementFactoryImpl();
        Story story = factory.createStory("Story Four by Four", "Description Story", Size.LARGE);
        Assert.assertTrue(story instanceof Story);
    }
}
