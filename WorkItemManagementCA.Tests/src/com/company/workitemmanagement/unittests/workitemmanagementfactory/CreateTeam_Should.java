package com.company.workitemmanagement.unittests.workitemmanagementfactory;

import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import com.company.workitemsmanagement.models.contracts.Team;
import org.junit.Assert;
import org.junit.Test;

public class CreateTeam_Should {

    @Test
    public void createTeam_Should_ReturnInstanceOfTypeTeam() {
        WorkItemManagementFactoryImpl factory = new WorkItemManagementFactoryImpl();
        Team team = factory.createTeam("Team 08");
        Assert.assertTrue(team instanceof Team);
    }
}
