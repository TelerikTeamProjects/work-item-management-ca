package com.company.workitemmanagement.unittests.workitemmanagementfactory;

import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import com.company.workitemsmanagement.models.contracts.Feedback;
import org.junit.Assert;
import org.junit.Test;

public class CreateFeedback_Should {

    @Test
    public void createFeedback_Should_ReturnInstanceOfTypeFeedback() {
        WorkItemManagementFactoryImpl factory = new WorkItemManagementFactoryImpl();
        Feedback feedback = factory.createFeedback("Feedback Two on One", "Description of Description" );
        Assert.assertTrue(feedback instanceof Feedback);
    }
}
