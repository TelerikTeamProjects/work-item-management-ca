package com.company.workitemmanagement.unittests.workitemmanagementfactory;

import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import com.company.workitemsmanagement.models.contracts.Bug;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class CreateBug_Should {

    @Test
    public void createBug_Should_ReturnInstanceOfTypeBug() {
        WorkItemManagementFactoryImpl factory = new WorkItemManagementFactoryImpl();
        Bug bug = factory.createBug("Bug Instance", "Bug Description", new ArrayList<>());
        Assert.assertTrue(bug instanceof Bug);
    }
}
