package com.company.workitemmanagement.unittests.workitem;

import com.company.workitemsmanagement.models.board.BoardImpl;
import com.company.workitemsmanagement.models.common.Size;
import com.company.workitemsmanagement.models.contracts.*;
import com.company.workitemsmanagement.models.person.PersonImpl;
import com.company.workitemsmanagement.models.team.TeamImpl;
import com.company.workitemsmanagement.models.workitem.*;
import org.junit.Test;

import java.util.ArrayList;

public class Constructor_Should {
    Board board;
    Bug bug;
    Feedback feedback;
    Person person;
    Story story;
    Team team;

    // Board tests
    @Test(expected = IllegalArgumentException.class)
    public void createBoard_Should_ThrowException_When_NameIsBelowMinLength() {
        board = new BoardImpl("Min");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createBoard_Should_ThrowException_When_NameIsAboveMaxLength() {
        board = new BoardImpl("Loooooooooooooooooooooooooooooooong title");
    }

    @Test
    public void createBoardWhenValidValuesArePassed() {
        board = new BoardImpl("Board1");
    }

    // Bug tests
    @Test(expected = IllegalArgumentException.class)
    public void createBug_Should_ThrowException_When_TitleIsBelowMinLength() {
        bug = new BugImpl("Min", "Test wih valid description", new ArrayList<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createBug_Should_ThrowException_When_TitleIsAboveMaxLength() {
        bug = new BugImpl("Extremely loooooooooooooooooooooooooooooooong title", "Test wih valid description", new ArrayList<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createBug_Should_ThrowException_When_DescriptionIsBelowMinLength() {
        bug = new BugImpl("Bug Title 123", "Below min", new ArrayList<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createBug_Should_ThrowException_When_DescriptionIsAboveMaxLength() {
        bug = new BugImpl("Bug Title 123", "Extremely loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong description", new ArrayList<>());
    }

    @Test
    public void createBug_Should_CreateBug_When_ValidValuesArePassed() {
        bug = new BugImpl("Bug title length", "Test valid wih valid parameters", new ArrayList<>());
    }

    // Feedback tests
    @Test(expected = IllegalArgumentException.class)
    public void createFeedback_Should_ThrowException_When_TitleIsBelowMinLength() {
        feedback = new FeedbackImpl("Min", "Test wih valid description");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createFeedback_Should_ThrowException_When_TitleIsAboveMaxLength() {
        feedback = new FeedbackImpl("Extremely loooooooooooooooooooooooooooooooong title", "Test wih valid description");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createFeedback_Should_ThrowException_When_DescriptionIsBelowMinLength() {
        feedback = new FeedbackImpl("Feedback Title 123", "Below min");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createFeedback_Should_ThrowException_When_DescriptionIsAboveMaxLength() {
        feedback = new FeedbackImpl("Feedback Title 123", "Extremely loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong description");
    }

    @Test
    public void createFeedback_Should_CreateFeedback_When_ValidValuesArePassed() {
        // Arrange, Act, Assert
        feedback = new FeedbackImpl("Feedback title", "Test feedback with valid parameters");
    }

    // Person tests
    @Test(expected = IllegalArgumentException.class)
    public void createPerson_Should_ThrowException_When_NameIsBelowMinLength() {
        person = new PersonImpl("Min");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createPerson_Should_ThrowException_When_NameIsAboveMaxLength() {
        person = new PersonImpl("Extremely loooooooooooooooooooooooooooooooong name");
    }

    @Test
    public void createPersonWhenValidValuesArePassed() {
        person = new PersonImpl("NikiS");
    }

    // Story tests
    @Test(expected = IllegalArgumentException.class)
    public void createStory_Should_ThrowException_When_TitleIsBelowMinLength() {
        story = new StoryImpl("Min", "Test wih valid description", Size.MEDIUM);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createStory_Should_ThrowException_When_TitleIsAboveMaxLength() {
        story = new StoryImpl("Extremely loooooooooooooooooooooooooooooooong title", "Test wih valid description", Size.SMALL);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createStory_Should_ThrowException_When_DescriptionIsBelowMinLength() {
        story = new StoryImpl("Story Title 123", "Below min", Size.LARGE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createStory_Should_ThrowException_When_DescriptionIsAboveMaxLength() {
        story = new StoryImpl("Bug Title 123", "Extremely loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong description", Size.LARGE);
    }

    @Test
    public void createStory_Should_CreateStory_When_ValidValuesArePassed() {
        story = new StoryImpl("Story title length", "Test story with valid parameters", Size.SMALL);
    }

    // Team tests
    @Test(expected = IllegalArgumentException.class)
    public void createTeam_Should_ThrowException_When_NameIsBelowMinLength() {
        team = new TeamImpl("Min");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createTeam_Should_ThrowException_When_NameIsAboveMaxLength() {
        team = new TeamImpl("Extremely loooooooooooooooooooooooooooooooong name");
    }

     @Test
    public void createTeamWhenValidValuesArePassed() {
        team = new TeamImpl("Team08");
    }
}

