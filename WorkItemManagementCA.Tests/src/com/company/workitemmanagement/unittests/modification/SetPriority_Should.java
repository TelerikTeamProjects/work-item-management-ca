package com.company.workitemmanagement.unittests.modification;

import com.company.workitemsmanagement.models.common.Priority;
import com.company.workitemsmanagement.models.common.Size;
import com.company.workitemsmanagement.models.contracts.Bug;
import com.company.workitemsmanagement.models.contracts.Story;
import com.company.workitemsmanagement.models.workitem.BugImpl;
import com.company.workitemsmanagement.models.workitem.StoryImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class SetPriority_Should {

    @Test
    public void setPriority_Should_ChangeRating_When_BugHigh() {
        Bug bug = new BugImpl("Bug Title Title", "Bug Description", new ArrayList<>());
        bug.setPriority(Priority.HIGH);
        Assert.assertEquals(Priority.HIGH, bug.getPriority());
    }

    @Test
    public void setPriority_Should_ChangeRating_When_BugLow() {
        Bug bug = new BugImpl("Bug Title Title", "Bug Description", new ArrayList<>());
        bug.setPriority(Priority.LOW);
        Assert.assertEquals(Priority.LOW, bug.getPriority());
    }

    @Test
    public void setPriority_Should_ChangeRating_When_BugMedium() {
        Bug bug = new BugImpl("Bug Title Title", "Bug Description", new ArrayList<>());
        bug.setPriority(Priority.MEDIUM);
        Assert.assertEquals(Priority.MEDIUM, bug.getPriority());
    }

    @Test
    public void setPriority_Should_ChangeRating_When_StoryMedium() {
        Story story = new StoryImpl("Story Title Title", "Story Description", Size.valueOf("SMALL"));
        story.setPriority(Priority.MEDIUM);
        Assert.assertEquals(Priority.MEDIUM, story.getPriority());
    }

    @Test
    public void setPriority_Should_ChangeRating_When_StoryHigh() {
        Story story = new StoryImpl("Story Title Title", "Story Description", Size.valueOf("SMALL"));
        story.setPriority(Priority.HIGH);
        Assert.assertEquals(Priority.HIGH, story.getPriority());
    }

    @Test
    public void setPriority_Should_ChangeRating_When_StoryLow() {
        Story story = new StoryImpl("Story Title Title", "Story Description", Size.valueOf("SMALL"));
        story.setPriority(Priority.LOW);
        Assert.assertEquals(Priority.LOW, story.getPriority());
    }
}
