package com.company.workitemmanagement.unittests.modification;

import com.company.workitemsmanagement.models.contracts.Person;
import com.company.workitemsmanagement.models.contracts.Team;
import com.company.workitemsmanagement.models.person.PersonImpl;
import com.company.workitemsmanagement.models.team.TeamImpl;

import org.junit.Assert;
import org.junit.Test;

public class AddMember_Should {

    @Test
    public void addMember_Should_AddMemberToTeam_When_ValidEntry() {
        Team team = new TeamImpl("Team Name");
        Person member = new PersonImpl("Name Valid");
        team.addMember(member);
        Assert.assertEquals(1, team.getMembers().size());
    }
}
