package com.company.workitemmanagement.unittests.modification;

import com.company.workitemsmanagement.models.board.BoardImpl;
import com.company.workitemsmanagement.models.common.Size;
import com.company.workitemsmanagement.models.contracts.*;
import com.company.workitemsmanagement.models.person.PersonImpl;
import com.company.workitemsmanagement.models.workitem.BugImpl;
import com.company.workitemsmanagement.models.workitem.FeedbackImpl;
import com.company.workitemsmanagement.models.workitem.StoryImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class AddWorkItem_Should {

    @Test
    public void addWorkItem_Should_AddItem_When_BoardItemIsValidBug() {
        Bug bug = new BugImpl("Bug Title Title", "Bug Description", new ArrayList<>());
        Board board = new BoardImpl("Board name");
        board.addWorkItem(bug);
        Assert.assertEquals(1, board.getWorkItems().size());
    }

    @Test
    public void addWorkItem_Should_AddItem_When_BoardItemIsValidStory() {
        Story story = new StoryImpl("Story Title Title", "Story Description", Size.SMALL);
        Board board = new BoardImpl("Board name");
        board.addWorkItem(story);
        Assert.assertEquals(1, board.getWorkItems().size());
    }

    @Test
    public void addWorkItem_Should_AddItem_When_BoardItemIsValidFeedback() {
        Feedback feedback = new FeedbackImpl("Feedback Title", "Feedback Description");
        Board board = new BoardImpl("Board name");
        board.addWorkItem(feedback);
        Assert.assertEquals(1, board.getWorkItems().size());
    }

    @Test
    public void addWorkItem_Should_AddItem_When_PersonItemIsValidBug() {
        Bug bug = new BugImpl("Bug Title Title", "Bug Description", new ArrayList<>());
        Person person = new PersonImpl("Person");
        person.addWorkItem(bug);
        Assert.assertEquals(1, person.getWorkItems().size());
    }

    @Test
    public void addWorkItem_Should_AddItem_When_PersonItemIsValidStory() {
        Story story = new StoryImpl("Story Title Title", "Story Description", Size.SMALL);
        Person person = new PersonImpl("Person");
        person.addWorkItem(story);
        Assert.assertEquals(1, person.getWorkItems().size());
    }

    @Test
    public void addWorkItem_Should_AddItem_When_PersonItemIsValidFeedback() {
        Feedback feedback = new FeedbackImpl("Feedback Title", "Feedback Description");
        Person person = new PersonImpl("Person");
        person.addWorkItem(feedback);
        Assert.assertEquals(1, person.getWorkItems().size());
    }


}
