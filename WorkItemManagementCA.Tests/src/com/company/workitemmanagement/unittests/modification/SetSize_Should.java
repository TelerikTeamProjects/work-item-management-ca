package com.company.workitemmanagement.unittests.modification;


import com.company.workitemsmanagement.models.common.Size;
import com.company.workitemsmanagement.models.contracts.Story;
import com.company.workitemsmanagement.models.workitem.StoryImpl;
import org.junit.Assert;
import org.junit.Test;

public class SetSize_Should {

    @Test
    public void changeSize_Should_ChangeSize_When_SizeIsLarge() {
        Story story = new StoryImpl("Story Title", "Description", Size.SMALL);
        story.setSize(Size.LARGE);
        Assert.assertEquals(Size.LARGE, story.getSize());
    }

    @Test
    public void changeSize_Should_ChangeSize_When_SizeIsMedium() {
        Story story = new StoryImpl("Story Title", "Description", Size.SMALL);
        story.setSize(Size.MEDIUM);
        Assert.assertEquals(Size.MEDIUM, story.getSize());
    }

    @Test
    public void changeSize_Should_ChangeSize_When_SizeIsSmall() {
        Story story = new StoryImpl("Story Title", "Description", Size.SMALL);
        story.setSize(Size.SMALL);
        Assert.assertEquals(Size.SMALL, story.getSize());
    }
}
