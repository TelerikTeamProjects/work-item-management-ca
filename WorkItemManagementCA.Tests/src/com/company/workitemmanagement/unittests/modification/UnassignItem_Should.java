package com.company.workitemmanagement.unittests.modification;

import com.company.workitemsmanagement.models.common.Size;
import com.company.workitemsmanagement.models.contracts.Person;
import com.company.workitemsmanagement.models.contracts.Story;
import com.company.workitemsmanagement.models.person.PersonImpl;
import com.company.workitemsmanagement.models.workitem.StoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.company.workitemsmanagement.core.EngineImpl.DEFAULT_PERSON;


public class UnassignItem_Should {
    private Story story;

    @Before
    public void initialize() {
        story = new StoryImpl("Story Title", "Description", Size.MEDIUM);
    }

    @Test
    public void unassignItem_ShouldNot_ThrowException_When_ItemIsNotAssigned() {
        story.unassignItem();
        Assert.assertEquals(DEFAULT_PERSON.getName(), story.getAssignee().getName());
    }

    @Test
    public void unassignItem_Should_UnassignItem_When_ItemIsAssigned() {
        Person person = new PersonImpl("Assignee");

        story.setAssignee(person);
        Assert.assertEquals(person, story.getAssignee());

        story.unassignItem();
        Assert.assertEquals(DEFAULT_PERSON.getName(), story.getAssignee().getName());
    }
}
