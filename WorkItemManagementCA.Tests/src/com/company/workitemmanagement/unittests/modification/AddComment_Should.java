package com.company.workitemmanagement.unittests.modification;

import com.company.workitemsmanagement.models.contracts.WorkItem;
import com.company.workitemsmanagement.models.workitem.FeedbackImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AddComment_Should {
    private WorkItem feedback;

    @Before
    public void initialize() {
        feedback = new FeedbackImpl("Feedback Title", "Description");
    }

    @Test
    public void addComment_Should_AddComment_When_CommentIsEmptyString() {
        feedback.addComment("");
        Assert.assertEquals(1, feedback.getComments().size());
    }

    @Test
    public void addComment_Should_AddComment_When_CommentIsValidString() {
        feedback.addComment("Add a comment");
        Assert.assertEquals(1, feedback.getComments().size());
    }
}
