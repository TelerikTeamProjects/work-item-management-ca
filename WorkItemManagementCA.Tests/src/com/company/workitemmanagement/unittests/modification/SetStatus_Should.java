package com.company.workitemmanagement.unittests.modification;

import com.company.workitemsmanagement.models.common.Size;
import com.company.workitemsmanagement.models.common.StatusBug;
import com.company.workitemsmanagement.models.common.StatusFeedback;
import com.company.workitemsmanagement.models.common.StatusStory;
import com.company.workitemsmanagement.models.contracts.Bug;
import com.company.workitemsmanagement.models.contracts.Feedback;
import com.company.workitemsmanagement.models.contracts.Story;
import com.company.workitemsmanagement.models.workitem.BugImpl;
import com.company.workitemsmanagement.models.workitem.FeedbackImpl;
import com.company.workitemsmanagement.models.workitem.StoryImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class SetStatus_Should {

    @Test
    public void setStatus_Should_SetStatus_When_BugActive() {
        Bug bug = new BugImpl("Bug Title Title", "Bug Description", new ArrayList<>());
        bug.setStatus(StatusBug.ACTIVE);
        Assert.assertEquals(StatusBug.ACTIVE,bug.getStatus());
    }

    @Test
    public void setStatus_Should_SetStatus_When_BugFixed() {
        Bug bug = new BugImpl("Bug Title Title", "Bug Description", new ArrayList<>());
        bug.setStatus(StatusBug.FIXED);
        Assert.assertEquals(StatusBug.FIXED,bug.getStatus());
    }

    @Test
    public void setStatus_Should_SetStatus_When_StoryNotDone() {
        Story story = new StoryImpl("Story Title", "Description", Size.SMALL);
        story.setStatus(StatusStory.NOT_DONE);
        Assert.assertEquals(StatusStory.NOT_DONE,story.getStatus());
    }

    @Test
    public void setStatus_Should_SetStatus_When_StoryDone() {
        Story story = new StoryImpl("Story Title", "Description", Size.SMALL);
        story.setStatus(StatusStory.DONE);
        Assert.assertEquals(StatusStory.DONE,story.getStatus());
    }

    @Test
    public void setStatus_Should_SetStatus_When_StoryInProgress() {
        Story story = new StoryImpl("Story Title", "Description", Size.SMALL);
        story.setStatus(StatusStory.IN_PROGRESS);
        Assert.assertEquals(StatusStory.IN_PROGRESS,story.getStatus());
    }

    @Test
    public void setStatus_Should_SetStatus_When_FeedbackNew() {
        Feedback feedback = new FeedbackImpl("Feedback Title", "Description");
        feedback.setStatus(StatusFeedback.NEW);
        Assert.assertEquals(StatusFeedback.NEW, feedback.getStatus());
    }

    @Test
    public void setStatus_Should_SetStatus_When_FeedbackDone() {
        Feedback feedback = new FeedbackImpl("Feedback Title", "Description");
        feedback.setStatus(StatusFeedback.DONE);
        Assert.assertEquals(StatusFeedback.DONE, feedback.getStatus());
    }

    @Test
    public void setStatus_Should_SetStatus_When_FeedbackScheduled() {
        Feedback feedback = new FeedbackImpl("Feedback Title", "Description");
        feedback.setStatus(StatusFeedback.SCHEDULED);
        Assert.assertEquals(StatusFeedback.SCHEDULED, feedback.getStatus());
    }

    @Test
    public void setStatus_Should_SetStatus_When_FeedbackUnscheduled() {
        Feedback feedback = new FeedbackImpl("Feedback Title", "Description");
        feedback.setStatus(StatusFeedback.UNSCHEDULED);
        Assert.assertEquals(StatusFeedback.UNSCHEDULED, feedback.getStatus());
    }
}
