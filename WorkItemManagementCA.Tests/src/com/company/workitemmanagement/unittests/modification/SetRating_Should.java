package com.company.workitemmanagement.unittests.modification;

import com.company.workitemsmanagement.models.contracts.Feedback;
import com.company.workitemsmanagement.models.workitem.FeedbackImpl;
import org.junit.Assert;
import org.junit.Test;

public class SetRating_Should {

    @Test
    public void setRating_Should_ChangeRating_When_RatingIsValidPositiveNumber() {
        Feedback feedback = new FeedbackImpl("Feedback Title", "Description");
        feedback.setRating(9);
        Assert.assertEquals(9, feedback.getRating());
    }

    @Test
    public void setRating_Should_ChangeRating_When_RatingIsValidNegativeNumber() {
        Feedback feedback = new FeedbackImpl("Feedback Title", "Description");
        feedback.setRating(-9);
        Assert.assertEquals(-9, feedback.getRating());
    }

    @Test (expected = IllegalArgumentException.class)
    public void setRating_Should_ChangeRating_When_RatingIsBelowTarget() {
        Feedback feedback = new FeedbackImpl("Feedback Title", "Description");
        feedback.setRating(-111);
    }

    @Test (expected = IllegalArgumentException.class)
    public void setRating_Should_ChangeRating_When_RatingIsAboveTarget() {
        Feedback feedback = new FeedbackImpl("Feedback Title", "Description");
        feedback.setRating(111);
    }

}
