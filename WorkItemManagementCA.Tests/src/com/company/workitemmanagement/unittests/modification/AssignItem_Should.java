package com.company.workitemmanagement.unittests.modification;

import com.company.workitemsmanagement.models.contracts.Bug;
import com.company.workitemsmanagement.models.contracts.Person;
import com.company.workitemsmanagement.models.person.PersonImpl;
import com.company.workitemsmanagement.models.workitem.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class AssignItem_Should {
    private Bug bug;

    @Before
    public void initialize() {
        bug = new BugImpl("Feedback Title", "Description", new ArrayList<>());
    }

    @Test
    public void assignItem_Should_ThrowException_When_AssigneePersonIsNull() {
        bug.setAssignee(null);
        Assert.assertEquals(null, bug.getAssignee());
    }

    @Test
    public void assignItem_Should_AssignItem_When_AssigneePersonIsValid() {
        Person person = new PersonImpl("Assignee");
        bug.setAssignee(person);
        Assert.assertEquals(person, bug.getAssignee());
    }

}
