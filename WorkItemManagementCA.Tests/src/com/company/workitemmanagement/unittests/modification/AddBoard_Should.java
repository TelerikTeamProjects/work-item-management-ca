package com.company.workitemmanagement.unittests.modification;

import com.company.workitemsmanagement.models.board.BoardImpl;
import com.company.workitemsmanagement.models.contracts.Board;
import com.company.workitemsmanagement.models.contracts.Team;
import com.company.workitemsmanagement.models.team.TeamImpl;
import org.junit.Assert;
import org.junit.Test;

public class AddBoard_Should {

    @Test
    public void addBoard_Should_AddBoardToTeam_When_ValidEntry() {
        Board board = new BoardImpl("Board");
        Team team = new TeamImpl("Team Name");
        team.addBoard(board);
        Assert.assertEquals(1, team.getBoards().size());
    }
}
