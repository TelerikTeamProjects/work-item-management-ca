package com.company.workitemmanagement.unittests.modification;

import com.company.workitemsmanagement.models.common.Severity;
import com.company.workitemsmanagement.models.contracts.Bug;
import com.company.workitemsmanagement.models.workitem.BugImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class SetSeverity_Should {

    @Test
    public void setSeverity_Should_ChangeRating_When_SeverityIsMajor() {
        Bug bug = new BugImpl("Title BugDeBug", "Description Bug", new ArrayList<>());
        bug.setSeverity(Severity.valueOf("MAJOR"));
        Assert.assertEquals(Severity.MAJOR, bug.getSeverity());
    }

    @Test
    public void setSeverity_Should_ChangeRating_When_SeverityIsMinor() {
        Bug bug = new BugImpl("Title BugDeBug", "Description Bug", new ArrayList<>());
        bug.setSeverity(Severity.valueOf("MINOR"));
        Assert.assertEquals(Severity.MINOR, bug.getSeverity());
    }

    @Test
    public void setSeverity_Should_ChangeRating_When_SeverityIsCritical() {
        Bug bug = new BugImpl("Title BugDeBug", "Description Bug", new ArrayList<>());
        bug.setSeverity(Severity.valueOf("CRITICAL"));
        Assert.assertEquals(Severity.CRITICAL, bug.getSeverity());
    }
}