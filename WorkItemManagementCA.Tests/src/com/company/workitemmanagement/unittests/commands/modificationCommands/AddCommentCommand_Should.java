package com.company.workitemmanagement.unittests.commands.modificationCommands;

import com.company.workitemsmanagement.commands.creation.CreateBoardCommand;
import com.company.workitemsmanagement.commands.creation.CreateFeedbackCommand;
import com.company.workitemsmanagement.commands.modification.AddCommentCommand;
import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import com.company.workitemsmanagement.models.team.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class AddCommentCommand_Should {
    WorkItemManagementFactory factory;
    Engine engine;
    AddCommentCommand addCommentCommand;

    @Before
    public void initialize() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
        addCommentCommand = new AddCommentCommand(factory, engine);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addCommentExecute_Should_ThrowException_When_EmptyParametersListIsPassed(){
        addCommentCommand.execute(new ArrayList<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void addCommentExecute_Should_ThrowException_When_WorkItemDoesntExist(){
        addCommentCommand.execute(Arrays.asList("0", "Comment"));
    }

    @Test
    public void addCommentExecute_Should_AddComment_When_ValidParametersListIsPassed() {
        engine.getTeams().add(new TeamImpl("Team Name"));
        CreateBoardCommand createBoardCommand = new CreateBoardCommand(factory, engine);
        createBoardCommand.execute(Arrays.asList("Board", "0"));
        CreateFeedbackCommand createFeedbackCommand = new CreateFeedbackCommand(factory, engine);
        createFeedbackCommand.execute(Arrays.asList("Feedback Title", "Feedback description","0"));

        addCommentCommand.execute(Arrays.asList("1", "Comment"));
        Assert.assertEquals(2, engine.getWorkItems().get(1).getHistory().size());
    }
}
