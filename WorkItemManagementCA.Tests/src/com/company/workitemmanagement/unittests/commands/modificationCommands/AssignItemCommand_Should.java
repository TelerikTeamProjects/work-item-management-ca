package com.company.workitemmanagement.unittests.commands.modificationCommands;

import com.company.workitemsmanagement.commands.creation.CreateBoardCommand;
import com.company.workitemsmanagement.commands.creation.CreateBugCommand;
import com.company.workitemsmanagement.commands.creation.CreatePersonCommand;
import com.company.workitemsmanagement.commands.modification.AddTeamMemberCommand;
import com.company.workitemsmanagement.commands.modification.AssignItemCommand;
import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import com.company.workitemsmanagement.models.contracts.Bug;
import com.company.workitemsmanagement.models.team.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class AssignItemCommand_Should {
    WorkItemManagementFactory factory;
    Engine engine;

    @Before
    public void initialize() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
    }

    @Test
    public void assignItemExecute_Should_AssignItemToMember_When_ValidParametersListIsPassed() {
        engine.getTeams().add(new TeamImpl("Team Name"));
        CreateBoardCommand createBoardCommand = new CreateBoardCommand(factory, engine);
        createBoardCommand.execute(Arrays.asList("Board", "0"));
        CreateBugCommand createBugCommand = new CreateBugCommand(factory, engine);
        createBugCommand.execute(Arrays.asList("Bug title 01", "Bug Description", "Steps1;Steps2", "0"));

        CreatePersonCommand createPersonCommand = new CreatePersonCommand(factory,engine);
        createPersonCommand.execute(Arrays.asList("Person"));

        AddTeamMemberCommand addTeamMemberCommand = new AddTeamMemberCommand(factory,engine);
        addTeamMemberCommand.execute(Arrays.asList("0", "Person"));

        AssignItemCommand assignItemCommand = new AssignItemCommand(factory, engine);
        assignItemCommand.execute(Arrays.asList("1","Person"));

        Assert.assertEquals("Person",((Bug) engine.getWorkItems().get(1)).getAssignee().getName());
    }
}
