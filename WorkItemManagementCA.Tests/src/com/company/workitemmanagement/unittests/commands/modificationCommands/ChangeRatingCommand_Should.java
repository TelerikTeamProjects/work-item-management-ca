package com.company.workitemmanagement.unittests.commands.modificationCommands;


import com.company.workitemsmanagement.commands.creation.CreateBoardCommand;
import com.company.workitemsmanagement.commands.creation.CreateFeedbackCommand;
import com.company.workitemsmanagement.commands.creation.CreateStoryCommand;
import com.company.workitemsmanagement.commands.modification.ChangeRatingCommand;
import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;

import com.company.workitemsmanagement.models.contracts.Feedback;
import com.company.workitemsmanagement.models.team.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;


public class ChangeRatingCommand_Should {
    WorkItemManagementFactory factory;
    Engine engine;
    ChangeRatingCommand changeRatingCommand;

    @Before
    public void initialize() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
        changeRatingCommand = new ChangeRatingCommand(factory, engine);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeRatingExecute_Should_ThrowException_When_EmptyParametersListIsPassed(){
        changeRatingCommand.execute(new ArrayList<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeRatingExecute_Should_ThrowException_When_WorkItemDoesntExist(){
        changeRatingCommand.execute(Arrays.asList("0", "7"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeRatingExecute_Should_ChangeRating_When_IncompatibleItemTypeIsPassed() {
        engine.getTeams().add(new TeamImpl("Team Name"));
        CreateBoardCommand createBoardCommand = new CreateBoardCommand(factory, engine);
        createBoardCommand.execute(Arrays.asList("Board", "0"));
        CreateStoryCommand createStoryCommand = new CreateStoryCommand(factory, engine);
        createStoryCommand.execute(Arrays.asList("Story Title", "Story description","Medium","0"));

        changeRatingCommand.execute(Arrays.asList("1", "8"));
    }

    @Test
    public void changeRatingExecute_Should_CreateTeam_When_ValidParametersListIsPassed() {
        engine.getTeams().add(new TeamImpl("Team Name"));
        CreateBoardCommand createBoardCommand = new CreateBoardCommand(factory, engine);
        createBoardCommand.execute(Arrays.asList("Board", "0"));
        CreateFeedbackCommand createFeedbackCommand = new CreateFeedbackCommand(factory, engine);
        createFeedbackCommand.execute(Arrays.asList("Feedback Title", "Feedback description","0"));

        changeRatingCommand.execute(Arrays.asList("1", "8"));
        Assert.assertEquals(8, ((Feedback) engine.getWorkItems().get(1)).getRating());
    }
}
