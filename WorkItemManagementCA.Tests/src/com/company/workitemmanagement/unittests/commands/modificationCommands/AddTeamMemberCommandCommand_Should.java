package com.company.workitemmanagement.unittests.commands.modificationCommands;

import com.company.workitemsmanagement.commands.creation.CreatePersonCommand;
import com.company.workitemsmanagement.commands.modification.AddTeamMemberCommand;
import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import com.company.workitemsmanagement.models.team.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class AddTeamMemberCommandCommand_Should {
    WorkItemManagementFactory factory;
    Engine engine;
    AddTeamMemberCommand addTeamMemberCommand;


    @Before
    public void initialize() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
        addTeamMemberCommand = new AddTeamMemberCommand(factory,engine);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addTeamMemberExecute_Should_ThrowException_When_EmptyParametersListIsPassed(){
        addTeamMemberCommand.execute(new ArrayList<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void addTeamMemberExecute_Should_AddTeamMembe_When_NonexistingPersonIsPassed(){
        engine.getTeams().add(new TeamImpl("Team Name"));
        addTeamMemberCommand.execute(Arrays.asList("0", "Person"));
    }

    @Test
    public void addTeamMemberExecute_Should_AddTeamMember_When_ValidParameterListsIsPassed(){
        engine.getTeams().add(new TeamImpl("Team Name"));
        CreatePersonCommand createPersonCommand = new CreatePersonCommand(factory,engine);
        createPersonCommand.execute(Arrays.asList("Person"));

        addTeamMemberCommand.execute(Arrays.asList("0", "Person"));

        Assert.assertEquals("Person", engine.getTeams().get(0).getMembers().get(0).getName());
    }
}
