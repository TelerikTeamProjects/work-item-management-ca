package com.company.workitemmanagement.unittests.commands.creationCommands;

import com.company.workitemsmanagement.commands.creation.CreateBoardCommand;
import com.company.workitemsmanagement.commands.creation.CreateFeedbackCommand;
import com.company.workitemsmanagement.commands.creation.CreateStoryCommand;
import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import com.company.workitemsmanagement.models.contracts.Story;
import com.company.workitemsmanagement.models.team.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class CreateStoryCommand_Should {
    WorkItemManagementFactory factory;
    Engine engine;
    CreateStoryCommand createStoryCommand;

    @Before
    public void initialize() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
        createStoryCommand = new CreateStoryCommand(factory, engine);
    }

    @Test (expected = IllegalArgumentException.class)
    public void createStoryExecute_Should_ThrowException_When_EmptyParametersListIsPassed() {
        createStoryCommand.execute(new ArrayList<>());
    }

    @Test (expected = IllegalArgumentException.class)
    public void createStoryExecute_Should_ThrowException_When_NonexistingBoardIsPassed(){
        createStoryCommand.execute(Arrays.asList("Story Title", "Story  description","Medium","0"));
    }

    @Test (expected = IllegalArgumentException.class)
    public void createStoryExecute_Should_CreateStory_When_InvalidSizeIsPassed(){
        engine.getTeams().add(new TeamImpl("Team Name"));
        CreateBoardCommand createBoardCommand = new CreateBoardCommand(factory, engine);
        createBoardCommand.execute(Arrays.asList("Board", "0"));

        createStoryCommand.execute(Arrays.asList("Story Title", "Story description","Medium2","0"));
    }

    @Test
    public void createStoryExecute_Should_CreateStory_When_ValidParametersListIsPassed(){
        engine.getTeams().add(new TeamImpl("Team Name"));
        CreateBoardCommand createBoardCommand = new CreateBoardCommand(factory, engine);
        createBoardCommand.execute(Arrays.asList("Board", "0"));

        createStoryCommand.execute(Arrays.asList("Story Title", "Story description","Medium","0"));
        Assert.assertEquals("Story Title", engine.getWorkItems().get(1).getTitle());
        Assert.assertEquals(1, engine.getWorkItems().get(1).getHistory().size());
        Assert.assertEquals("Medium", ((Story)engine.getWorkItems().get(1)).getPriority().toString());
        Assert.assertEquals("Not done", ((Story)engine.getWorkItems().get(1)).getStatus().toString());
        Assert.assertEquals("Medium", ((Story)engine.getWorkItems().get(1)).getSize().toString());
    }
}
