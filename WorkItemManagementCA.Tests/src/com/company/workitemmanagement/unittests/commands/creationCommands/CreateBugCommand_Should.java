package com.company.workitemmanagement.unittests.commands.creationCommands;

import com.company.workitemsmanagement.commands.creation.CreateBoardCommand;
import com.company.workitemsmanagement.commands.creation.CreateBugCommand;
import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import com.company.workitemsmanagement.models.contracts.Bug;
import com.company.workitemsmanagement.models.team.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class CreateBugCommand_Should {
    WorkItemManagementFactory factory;
    Engine engine;
    CreateBugCommand createBugCommand;

    @Before
    public void initialize() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
        createBugCommand = new CreateBugCommand(factory, engine);
    }

    @Test (expected = IllegalArgumentException.class)
    public void createBugExecute_Should_ThrowException_When_EmptyParametersListIsPassed() {
        createBugCommand.execute(new ArrayList<>());
    }

    @Test (expected = IllegalArgumentException.class)
    public void createBugExecute_Should_ThrowException_When_NonexistingBoardIsPassed(){
        createBugCommand.execute(Arrays.asList("Bug title 01", "Bug Description", "Steps1;Steps2", "0"));
    }

    @Test
    public void createBugExecute_Should_CreateBoard_When_ValidParametersListIsPassed(){
        engine.getTeams().add(new TeamImpl("Team Name"));
        CreateBoardCommand createBoardCommand = new CreateBoardCommand(factory, engine);
        createBoardCommand.execute(Arrays.asList("Board", "0"));

        createBugCommand.execute(Arrays.asList("Bug title 01", "Bug Description", "Steps1;Steps2", "0"));
        Assert.assertEquals("Bug title 01", engine.getWorkItems().get(1).getTitle());
        Assert.assertEquals(1, engine.getWorkItems().get(1).getHistory().size());
        Assert.assertEquals("Medium", ((Bug)engine.getWorkItems().get(1)).getPriority().toString());
        Assert.assertEquals("Active", ((Bug)engine.getWorkItems().get(1)).getStatus().toString());
        Assert.assertEquals("Minor", ((Bug)engine.getWorkItems().get(1)).getSeverity().toString());
    }
}
