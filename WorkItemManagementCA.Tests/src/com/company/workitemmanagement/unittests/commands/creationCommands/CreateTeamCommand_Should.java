package com.company.workitemmanagement.unittests.commands.creationCommands;

import com.company.workitemsmanagement.commands.creation.CreateTeamCommand;
import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class CreateTeamCommand_Should {
    WorkItemManagementFactory factory;
    Engine engine;
    CreateTeamCommand createTeamCommand;

    @Before
    public void initialize() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
        createTeamCommand = new CreateTeamCommand(factory, engine);
    }

    @Test (expected = IllegalArgumentException.class)
    public void createTeamExecute_Should_ThrowException_When_EmptyParametersListIsPassed() {
        createTeamCommand.execute(new ArrayList<>());
    }

    @Test
    public void createTeamExecute_Should_CreateTeam_When_ValidParametersListIsPassed() {
        createTeamCommand.execute(Arrays.asList("Team 08"));
        Assert.assertEquals("Team 08", engine.getTeams().get(0).getName());
    }
}
