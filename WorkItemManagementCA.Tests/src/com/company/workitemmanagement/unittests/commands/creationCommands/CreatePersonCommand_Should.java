package com.company.workitemmanagement.unittests.commands.creationCommands;

import com.company.workitemsmanagement.commands.creation.CreatePersonCommand;
import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class CreatePersonCommand_Should {
    WorkItemManagementFactory factory;
    Engine engine;
    CreatePersonCommand createPersonCommand;

    @Before
    public void initialize() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
        createPersonCommand = new CreatePersonCommand(factory, engine);
    }

    @Test (expected = IllegalArgumentException.class)
    public void createPersonExecute_Should_ThrowException_When_EmptyParametersListIsPassed() {
        createPersonCommand.execute(new ArrayList<>());
    }

    @Test (expected = IllegalArgumentException.class)
    public void createPersonExecute_Should_ThrowException_When_DuplicatePersonNameIsPassed() {
        createPersonCommand.execute(Arrays.asList("Person 1"));
        createPersonCommand.execute(Arrays.asList("Person 1"));
    }

    @Test
    public void createPersonExecute_Should_CreatePerson_When_ValidParametersListIsPassed() {
        createPersonCommand.execute(Arrays.asList("Person 1"));
        Assert.assertEquals("Person 1", engine.getPersons().get("Person 1").getName());
        Assert.assertEquals(1, engine.getPersons().get("Person 1").getActivityHistory().size());
    }
}
