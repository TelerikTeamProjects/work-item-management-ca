package com.company.workitemmanagement.unittests.commands.creationCommands;

import com.company.workitemsmanagement.commands.creation.CreateBoardCommand;
import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import com.company.workitemsmanagement.models.team.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class CreateBoardCommand_Should {
    WorkItemManagementFactory factory;
    Engine engine;
    CreateBoardCommand createBoardCommand;

    @Before
    public void initialize() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
        createBoardCommand = new CreateBoardCommand(factory, engine);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createBoardExecute_Should_ThrowException_When_EmptyParametersListIsPassed(){
        createBoardCommand.execute(new ArrayList<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createBoardExecute_Should_ThrowException_When_ValidParameterListsIsPassed(){
        createBoardCommand.execute(Arrays.asList("Board", "0"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createBoardExecute_Should_ThrowException_When_ExistingBoardIsPassedToTeam(){
        engine.getTeams().add(new TeamImpl("Team Name"));
        createBoardCommand.execute(Arrays.asList("Board", "0"));
        createBoardCommand.execute(Arrays.asList("Board", "0"));
    }

    @Test
    public void createBoardExecute_Should_CreateBoard_When_ValidParameterListsIsPassed(){
        engine.getTeams().add(new TeamImpl("Team Name"));
        createBoardCommand.execute(Arrays.asList("Board", "0"));
        Assert.assertEquals("Board", engine.getBoards().get(0).getName());
        Assert.assertEquals(1, engine.getBoards().get(0).getActivityHistory().size());
    }
}
