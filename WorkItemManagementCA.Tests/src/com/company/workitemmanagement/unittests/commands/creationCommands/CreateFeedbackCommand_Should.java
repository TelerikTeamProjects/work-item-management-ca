package com.company.workitemmanagement.unittests.commands.creationCommands;

import com.company.workitemsmanagement.commands.creation.CreateBoardCommand;
import com.company.workitemsmanagement.commands.creation.CreateFeedbackCommand;
import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import com.company.workitemsmanagement.models.contracts.Feedback;
import com.company.workitemsmanagement.models.team.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class CreateFeedbackCommand_Should {
    WorkItemManagementFactory factory;
    Engine engine;
    CreateFeedbackCommand createFeedbackCommand;

    @Before
    public void initialize() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
        createFeedbackCommand = new CreateFeedbackCommand(factory, engine);
    }

    @Test (expected = IllegalArgumentException.class)
    public void createFeedbackExecute_Should_ThrowException_When_EmptyParametersListIsPassed() {
        createFeedbackCommand.execute(new ArrayList<>());
    }

    @Test (expected = IllegalArgumentException.class)
    public void createFeedbackExecute_Should_ThrowException_When_NonexistingBoardIsPassed(){
       createFeedbackCommand.execute(Arrays.asList("Feedback Title", "Feedback description","0"));
    }

    @Test
    public void createFeedbackExecute_Should_CreateBoard_When_ValidParametersListIsPassed(){
        engine.getTeams().add(new TeamImpl("Team Name"));
        CreateBoardCommand createBoardCommand = new CreateBoardCommand(factory, engine);
        createBoardCommand.execute(Arrays.asList("Board", "0"));

        createFeedbackCommand.execute(Arrays.asList("Feedback Title", "Feedback description","0"));
        Assert.assertEquals("Feedback Title", engine.getWorkItems().get(1).getTitle());
        Assert.assertEquals(1, engine.getWorkItems().get(1).getHistory().size());
        Assert.assertEquals("New", ((Feedback)engine.getWorkItems().get(1)).getStatus().toString());
    }
}
