package com.company.workitemmanagement.unittests.commands.listCommands;

import com.company.workitemsmanagement.commands.creation.CreateBoardCommand;
import com.company.workitemsmanagement.commands.creation.CreateTeamCommand;
import com.company.workitemsmanagement.commands.listing.ListTeamBoardsCommands;
import com.company.workitemsmanagement.commands.listing.ListTeamsCommand;
import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class ListTeamBoardsCommands_Should {
    WorkItemManagementFactory factory;
    Engine engine;

    @Before
    public void initialize() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
    }

    @Test
    public void listTeamBoardsExecute_Should_ListAllTeamBoards_When_ValidParametersListIsPassed() {
        CreateTeamCommand createTeamCommand = new CreateTeamCommand(factory,engine);
        createTeamCommand.execute(Arrays.asList("Team 08"));

        CreateBoardCommand createBoardCommand = new CreateBoardCommand(factory, engine);
        createBoardCommand.execute(Arrays.asList("Board 1", "0"));
        createBoardCommand.execute(Arrays.asList("Board 2", "0"));

        Assert.assertEquals(2, engine.getTeams().get(0).getBoards().size());

        ListTeamBoardsCommands listTeamBoardsCommands = new ListTeamBoardsCommands(factory,engine);
        String result = listTeamBoardsCommands.execute(Arrays.asList("0"));

        Assert.assertEquals("### Team boards list ###\r\n" +
                " Board: Board 2\r\n" +
                " Board: Board 1", result);
    }
}
