package com.company.workitemmanagement.unittests.commands.listCommands;

import com.company.workitemsmanagement.commands.creation.CreatePersonCommand;
import com.company.workitemsmanagement.commands.listing.ListPeopleCommand;
import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class ListPeopleCommand_Should {
    WorkItemManagementFactory factory;
    Engine engine;

    @Before
    public void initialize() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
    }

    @Test
    public void listPeopleExecute_Should_ListAllPersons_When_ValidParametersListIsPassed() {
        CreatePersonCommand createPersonCommand = new CreatePersonCommand(factory,engine);
        createPersonCommand.execute(Arrays.asList("Person 1"));
        createPersonCommand.execute(Arrays.asList("Person 2"));
        Assert.assertEquals(2, engine.getPersons().size());

        ListPeopleCommand listPeopleCommand = new ListPeopleCommand(factory,engine);
        String result = listPeopleCommand.execute(new ArrayList<>());

        Assert.assertEquals("### People list ###\r\n" +
                " Name: Person 1\r\n" +
                " Name: Person 2", result);
    }
}
