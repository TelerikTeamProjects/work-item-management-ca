package com.company.workitemmanagement.unittests.commands.listCommands;

import com.company.workitemsmanagement.commands.creation.CreateBoardCommand;
import com.company.workitemsmanagement.commands.creation.CreateBugCommand;
import com.company.workitemsmanagement.commands.creation.CreateFeedbackCommand;
import com.company.workitemsmanagement.commands.creation.CreateStoryCommand;
import com.company.workitemsmanagement.commands.listing.ListItemsFilterByTypeCommand;
import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import com.company.workitemsmanagement.models.team.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class ListItemsFilterByTypeComand_Should {
    WorkItemManagementFactory factory;
    Engine engine;

    @Before
    public void initialize() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
    }

    @Test
    public void listItemsFilterByTypeExecute_Should_PrintAllWorkItems_When_NoFilterIsPassed(){
        engine.getTeams().add(new TeamImpl("Team Name"));
        CreateBoardCommand createBoardCommand = new CreateBoardCommand(factory, engine);
        createBoardCommand.execute(Arrays.asList("Board", "0"));

        CreateBugCommand createBugCommand = new CreateBugCommand(factory, engine);
        createBugCommand.execute(Arrays.asList("Bug title 01", "Bug Description", "Steps1;Steps2", "0"));

        CreateFeedbackCommand createFeedbackCommand = new CreateFeedbackCommand(factory,engine);
        createFeedbackCommand.execute(Arrays.asList("Feedback Title", "Feedback description","0"));

        CreateStoryCommand createStoryCommand = new CreateStoryCommand(factory,engine);
        createStoryCommand.execute(Arrays.asList("Story Title", "Story description","Medium","0"));

        ListItemsFilterByTypeCommand listItemsFilterByTypeCommand = new ListItemsFilterByTypeCommand(factory, engine);
        String result = listItemsFilterByTypeCommand.execute(new ArrayList<>());


        Assert.assertEquals("#List work items no filters:\r\n" +
                " #Bug ID: 1\r\n" +
                " #Title: Bug title 01\r\n" +
                "   Description: Bug Description\r\n" +
                "   Priority: Medium\r\n" +
                "   Assignee: Unassigned\r\n" +
                "   Status: Active\r\n" +
                "   Severity: Minor\r\n" +
                " #Feedback ID: 2\r\n" +
                " #Title: Feedback Title\r\n" +
                "   Description: Feedback description\r\n" +
                "   Status: New\r\n" +
                "   Rating: 0\r\n" +
                " #Story ID: 3\r\n" +
                " #Title: Story Title\r\n" +
                "   Description: Story description\r\n" +
                "   Priority: Medium\r\n" +
                "   Assignee: Unassigned\r\n" +
                "   Status: Not done\r\n" +
                "   Size: Medium", result);
    }
}
