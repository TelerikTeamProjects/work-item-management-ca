package com.company.workitemmanagement.unittests.commands.listCommands;

import com.company.workitemsmanagement.commands.creation.CreateTeamCommand;
import com.company.workitemsmanagement.commands.listing.ListTeamsCommand;
import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class ListTeamsCommand_Should {
    WorkItemManagementFactory factory;
    Engine engine;

    @Before
    public void initialize() {
        factory = new WorkItemManagementFactoryImpl();
        engine = new EngineImpl(factory);
    }

    @Test
    public void listTeamsExecute_Should_ListAllTeams_When_ValidParametersListIsPassed() {
        CreateTeamCommand createTeamCommand = new CreateTeamCommand(factory,engine);
        createTeamCommand.execute(Arrays.asList("Team 08"));
        createTeamCommand.execute(Arrays.asList("Best team"));
        Assert.assertEquals(2, engine.getTeams().size());

        ListTeamsCommand listTeamsCommand = new ListTeamsCommand(factory,engine);
        String result = listTeamsCommand.execute(new ArrayList<>());

        Assert.assertEquals("#List teams:\r\n" +
                " Team Team 08\r\n" +
                " Team Best team", result);
    }
}
