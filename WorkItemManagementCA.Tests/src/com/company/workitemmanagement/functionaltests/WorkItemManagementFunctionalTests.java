package com.company.workitemmanagement.functionaltests;

import com.company.workitemsmanagement.core.EngineImpl;
import com.company.workitemsmanagement.core.contracts.Engine;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactory;
import com.company.workitemsmanagement.core.factories.WorkItemManagementFactoryImpl;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WorkItemManagementFunctionalTests {
    // The tests are located in the Tests folder
    // Each test has two files - "in" and "out"
    // The "in" file is the input
    // The "out" file is the expected output of your program


    @Test
    public void test_01_create_person_valid_tests() throws Exception {
        executeIOTest("01_create_person_valid_tests");
    }

    @Test
    public void test_02_create_person_validations() throws Exception {
        executeIOTest("02_create_person_validations");
    }

    @Test
    public void test_03_create_team_valid_tests() throws Exception {
        executeIOTest("03_create_team_valid_tests");
    }

    @Test
    public void test_04_create_team_validations() throws Exception {
        executeIOTest("04_create_team_validations");
    }

    @Test
    public void test_05_create_board_valid_tests() throws Exception {
        executeIOTest("05_create_board_valid_tests");
    }

    @Test
    public void test_06_create_board_validations() throws Exception {
        executeIOTest("06_create_board_validations");
    }

    @Test
    public void test_07_create_bug_valid_tests() throws Exception {
        executeIOTest("07_create_bug_valid_tests");
    }

    @Test
    public void test_08_create_bug_validations() throws Exception {
        executeIOTest("08_create_bug_validations");
    }

    @Test
    public void test_09_create_story_valid_tests() throws Exception {
        executeIOTest("09_create_story_valid_tests");
    }

    @Test
    public void test_10_create_story_validations() throws Exception {
        executeIOTest("10_create_story_validations");
    }

    @Test
    public void test_11_create_feedback_valid_tests() throws Exception {
        executeIOTest("11_create_feedback_valid_tests");
    }

    @Test
    public void test_12_create_feedback_validations() throws Exception {
        executeIOTest("12_create_feedback_validations");
    }

    @Test
    public void test_13_add_comment_valid_tests() throws Exception {
        executeIOTest("13_add_comment_valid_tests");
    }

    @Test
    public void test_14_add_comment_validations() throws Exception {
        executeIOTest("14_add_comment_validations");
    }

    @Test
    public void test_15_add_team_member_valid_tests() throws Exception {
        executeIOTest("15_add_team_member_valid_tests");
    }

    @Test
    public void test_16_add_team_member_validations() throws Exception {
        executeIOTest("16_add_team_member_validations");
    }

    @Test
    public void test_17_assign_item_valid_tests() throws Exception {
        executeIOTest("17_assign_item_valid_tests");
    }

    @Test
    public void test_18_assign_item_validations() throws Exception {
        executeIOTest("18_assign_item_validations");
    }

    @Test
    public void test_19_change_priority_valid_tests() throws Exception {
        executeIOTest("19_change_priority_valid_tests");
    }

    @Test
    public void test_20_change_priority_validations() throws Exception {
        executeIOTest("20_change_priority_validations");
    }

    @Test
    public void test_21_change_rating_valid_tests() throws Exception {
        executeIOTest("21_change_rating_valid_tests");
    }

    @Test
    public void test_22_change_rating_validations() throws Exception {
        executeIOTest("22_change_rating_validations");
    }

    @Test
    public void test_23_change_severity_valid_tests() throws Exception {
        executeIOTest("23_change_severity_valid_tests");
    }

    @Test
    public void test_24_change_severity_validations() throws Exception {
        executeIOTest("24_change_severity_validations");
    }

    @Test
    public void test_25_change_size_valid_tests() throws Exception {
        executeIOTest("25_change_size_valid_tests");
    }

    @Test
    public void test_26_change_size_validations() throws Exception {
        executeIOTest("26_change_size_validations");
    }

    @Test
    public void test_27_change_status_valid_tests() throws Exception {
        executeIOTest("27_change_status_valid_tests");
    }

    @Test
    public void test_28_change_status_validations() throws Exception {
        executeIOTest("28_change_status_validations");
    }

    @Test
    public void test_29_unassign_item_valid_tests() throws Exception {
        executeIOTest("29_unassign_item_valid_tests");
    }

    @Test
    public void test_30_unassign_item_validations() throws Exception {
        executeIOTest("30_unassign_item_validations");
    }

    // TODO 31,32

    @Test
    public void test_33_list_items_valid_tests() throws Exception {
        executeIOTest("33_list_items_valid_tests");
    }

    @Test
    public void test_34_list_items_validations() throws Exception {
        executeIOTest("34_list_items_validations");
    }

    @Test
    public void test_35_list_people_valid_tests() throws Exception {
        executeIOTest("35_list_people_valid_tests");
    }

    @Test
    public void test_36_list_people_validations() throws Exception {
        executeIOTest("36_list_people_validations");
    }

    // TODO 37,38

    @Test
    public void test_39_list_person_work_items_valid_tests() throws Exception {
        executeIOTest("39_list_person_work_items_valid_tests");
    }

    @Test
    public void test_40_list_person_work_items_validations() throws Exception {
        executeIOTest("40_list_person_work_items_validations");
    }

    // TODO 41,42

    @Test
    public void test_43_list_team_boards_valid_tests() throws Exception {
        executeIOTest("43_list_team_boards_valid_tests");
    }

    @Test
    public void test_44_list_team_boards_validations() throws Exception {
        executeIOTest("44_list_team_boards_validations");
    }

    @Test
    public void test_45_list_team_members_valid_tests() throws Exception {
        executeIOTest("45_list_team_members_valid_tests");
    }

    @Test
    public void test_46_list_team_members_validations() throws Exception {
        executeIOTest("46_list_team_members_validations");
    }

    @Test
    public void test_47_list_teams_valid_tests() throws Exception {
        executeIOTest("47_list_teams_valid_tests");
    }

    @Test
    public void test_48_list_teams_validations() throws Exception {
        executeIOTest("48_list_teams_validations");
    }

    @Test
    public void test_49_sort_items_valid_tests() throws Exception {
        executeIOTest("49_sort_items_valid_tests");
    }

    @Test
    public void test_50_sort_items_validations() throws Exception {
        executeIOTest("50_sort_items_validations");
    }


    @Test
    public void test_example_input() throws Exception {
        executeIOTest("example_input");
    }

    private void executeIOTest(String testNumber) throws Exception {
        String testInputFilePath = "./src/com/company/workitemmanagement/functionaltests/TestData/test." + testNumber + ".in.txt";
        System.out.println(testInputFilePath);
        InputStream testInput = new FileInputStream(testInputFilePath);
        System.setIn(testInput);

        ByteArrayOutputStream outputByteStream = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(outputByteStream, true);
        System.setOut(output);

        WorkItemManagementFactory factory = new WorkItemManagementFactoryImpl();
        Engine engine = new EngineImpl(factory);
        engine.start();

        String testOutputFilePath = "./src/com/company/workitemmanagement/functionaltests/TestData/test." + testNumber + ".out.txt";
        byte[] testOutputData = Files.readAllBytes(Paths.get(testOutputFilePath));
        String expected = new String(testOutputData, StandardCharsets.UTF_8);
        String actual = new String(outputByteStream.toByteArray(), StandardCharsets.UTF_8);

        Assert.assertEquals(expected.trim(), actual.trim());
    }
}
